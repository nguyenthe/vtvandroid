package zazu.mobile.zazutv.sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import zazu.mobile.zazutv.convert.ConvertUser;
import zazu.mobile.zazutv.model.details.react.like.Data;
import zazu.mobile.zazutv.model.login.LoginDB;

/**
 * Created by Admin on 10/12/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper implements LikeDAO, UserDAO {

    private static final int DATABASE_VERSION = 1;
    private static final String LOG = "DATABASE-HANDLER";
    // Database Name
    private static final String DATABASE_NAME = "zazufilmDB";

    // Contacts table name
    private static final String TABLE_INTRO = "intro";
    private static final String TABLE_CONTACTS = "token";
    private static final String TABLE_FAVORITE = "favorite";
    private static final String TABLE_LIKE = "like";
    private static final String TABLE_LINK = "link";
    private static final String TABLE_LEFT_MENU = "leftMenu";
    private static final String TABLE_USER = "user";
    private static final String TABLE_LOGIN = "login";
    // Contacts Intro Table
    private static final String KEY_STATUS = "status";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TOKEN = "token";

    // Columns name of favorite table
    private static final String KEY_ID_FAVORITE = "titleId";
    private static final String KEY_NEW_STATUS = "newStatus";

    // Columns name of like table
    private static final String KEY_ID_LIKE = "titleId";
    private static final String KEY_NEW_RATING = "newRating";

    // Columns name for link table
    private static final String KEY_ID_LINK = "titleId";
    private static final String KEY_LINK = "link";

    //column name for left menu
    private static final String KEY_POSITION = "position";

    // column name for login table
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    //Todo Create Tables
    private static final String CREATE_TOKEN_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
            + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TOKEN + " TEXT"
            + ")";

    private static final String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + KEY_ID + " INTEGER PRIMARY KEY," + ConvertUser.USER_ID + " INTEGER NOT NULL UNIQUE ," +
            ConvertUser.USER_AVATAR + " TEXT ," + ConvertUser.USER_EMAIL + " TEXT ," +
            ConvertUser.USER_NAME + " TEXT ," + ConvertUser.USER_PHONE_NUMBER + " TEXT , " + ConvertUser.USER_TOKEN + " TEXT "
            + ")";

    private static final String CREATE_FAVORITE_TABLE = "CREATE TABLE " + TABLE_FAVORITE + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_ID_FAVORITE + " INTEGER NOT NULL UNIQUE ," + KEY_NEW_STATUS + " INTEGER"
            + ")";

    private static final String CREATE_LIKE_TABLE = "CREATE TABLE " + TABLE_LIKE + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_ID_LIKE + " INTEGER NOT NULL UNIQUE ," + KEY_NEW_RATING + " INTEGER"
            + ")";

    private static final String CREATE_LINK_TABLE = "CREATE TABLE " + TABLE_LINK + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_ID_LINK + " INTEGER NOT NULL UNIQUE ," + KEY_LINK + " TEXT"
            + ")";

    private static final String CREATE_INTRO_TABLE = "CREATE TABLE " + TABLE_INTRO + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_STATUS + " INTEGER NOT NULL UNIQUE "
            + ")";

    private static final String CREATE_MENU_LEFT_TABLE = "CREATE TABLE " + TABLE_LEFT_MENU + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_POSITION + " INTEGER"
            + ")";

    //login table
    private static final String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_USERNAME + " TEXT ," + KEY_PASSWORD + " TEXT "
            + ")";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_INTRO_TABLE);
        db.execSQL(CREATE_TOKEN_TABLE);
        db.execSQL(CREATE_FAVORITE_TABLE);
        db.execSQL(CREATE_LIKE_TABLE);
        db.execSQL(CREATE_LINK_TABLE);
        db.execSQL(CREATE_MENU_LEFT_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_LOGIN_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIKE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEFT_MENU);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);
        // Create tables again
        onCreate(db);
    }

    //handler login table
    public void insertLogin(LoginDB loginDB) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, loginDB.getUsername()); //Contact Name
        values.put(KEY_PASSWORD, loginDB.getPassword()); //
        db.insert(TABLE_LOGIN, null, values);
        db.close(); // Closing database connection
    }

    public List<LoginDB> getAllLogin() {
        List<LoginDB> contactList = new ArrayList<LoginDB>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoginDB contact = new LoginDB();
                contact.setUsername(cursor.getString(1));
                contact.setPassword(cursor.getString(2));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }

    // handle with like table
    @Override
    public void write(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_LIKE, data.getTitleId()); //Contact Name
        values.put(KEY_NEW_RATING, data.getNewRating()); //
        db.insert(TABLE_LIKE, null, values);
        db.close(); // Closing database connection
    }

    @Override
    public List<Data> getAll() {
        return null;
    }

    @Override
    public Data getOne(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. build query
        Cursor cursor = db.query(TABLE_LIKE, new String[]{KEY_ID_LIKE,
                        KEY_NEW_RATING}, KEY_ID_LIKE + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Data data = new Data();
        data.setTitleId(Integer.parseInt(cursor.getString(0)));
        data.setNewRating(Integer.parseInt(cursor.getString(1)));
        return data;
    }

    @Override
    public void update(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID_LIKE, data.getTitleId()); // Contact Name
        values.put(KEY_NEW_RATING, data.getNewRating());

        db.update(TABLE_LIKE, values, KEY_ID_LIKE + " =?",
                new String[]{String.valueOf(data.getTitleId())});
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public boolean isTableExists(String tableName) {
        boolean isExist = false;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT count(*) FROM sqlite_master WHERE type = 'table' AND tbl_name = " + "'" + tableName + "'";
        Log.e(LOG, "check_exists: " + query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                isExist = true;
            }
            cursor.close();
        }
        return isExist;
    }

    @Override
    public int countData() {
        String countQuery = "SELECT  *  FROM " + TABLE_LIKE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        Log.e(LOG, "query : " + countQuery);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int countToken() {
        String countQuery = "SELECT  * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        Log.e(LOG, "query : " + countQuery);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    @Override
    public int countFilmById(int id) {
        String countQuery = "SELECT  *  FROM " + TABLE_LIKE + " WHERE " + KEY_ID_LIKE + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        Log.e(LOG, "query : " + countQuery);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    ///// User Part

    @Override
    public void insertUser(ConvertUser convertUser) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ConvertUser.USER_ID, convertUser.getId());
        values.put(ConvertUser.USER_AVATAR, convertUser.getAvatar());
        values.put(ConvertUser.USER_EMAIL, convertUser.getEmail());
        values.put(ConvertUser.USER_NAME, convertUser.getUserName());
        values.put(ConvertUser.USER_PHONE_NUMBER, convertUser.getPhoneNumber());
        values.put(ConvertUser.USER_TOKEN, convertUser.getToken());
        db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection
    }

    @Override
    public List<ConvertUser> getAllUser() {
        List<ConvertUser> contactList = new ArrayList<ConvertUser>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ConvertUser contact = new ConvertUser();
                contact.setId(cursor.getInt(1));
                contact.setAvatar(cursor.getString(2));
                contact.setEmail(cursor.getString(3));
                contact.setUserName(cursor.getString(4));
                contact.setPhoneNumber(cursor.getString(5));
                contact.setToken(cursor.getString(6));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }

    @Override
    public ConvertUser getOneUser(int id) {
        return null;
    }

    @Override
    public void deleteUser(int id) {

    }

    @Override
    public void deleteAllUser() {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_USER, null, null);
    }

    @Override
    public boolean isTableExistsUser(String tableName) {
        return false;
    }

    @Override
    public int countDataUser() {
        String countQuery = "SELECT  * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        Log.e(LOG, "query : " + countQuery);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}
