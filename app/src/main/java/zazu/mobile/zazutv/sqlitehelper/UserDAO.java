package zazu.mobile.zazutv.sqlitehelper;

import java.util.List;

import zazu.mobile.zazutv.convert.ConvertUser;

/**
 * Created by Admin on 11/22/2017.
 */

public interface UserDAO{
    public void insertUser(ConvertUser t);
    public List<ConvertUser> getAllUser();
    public ConvertUser getOneUser(int id);
    public void deleteUser(int id);
    public void deleteAllUser();
    public boolean isTableExistsUser(String tableName);
    public int countDataUser();
}
