package zazu.mobile.zazutv.sqlitehelper;

import java.util.List;

import zazu.mobile.zazutv.model.details.react.like.Data;

/**
 * Created by Admin on 11/22/2017.
 */

public interface LikeDAO{
    public void write(Data t);

    public List<Data> getAll();

    public Data getOne(int id);

    public void update(Data data);

    public void delete(int id);

    public void deleteAll();

    public boolean isTableExists(String tableName);
    public int countData();
    public int countFilmById(int id);
}
