package zazu.mobile.zazutv.sqlitehelper;

import java.util.List;

/**
 * Created by Admin on 11/22/2017.
 */

public interface LocalDAO<T> {
    public void write(T t);

    public List<T> getAll();

    public T getOne(int id);

    public void delete(int id);

    public void deleteAll();

    public boolean isTableExists(String tableName);
}
