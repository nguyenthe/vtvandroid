package zazu.mobile.zazutv.convert;

/**
 * Created by Admin on 11/17/2017.
 */

public class ConvertUser {

    public static final String USER_ID = "user_id";
    public static final String USER_TOKEN = "token";
    public static final String USER_EMAIL = "email";
    public static final String USER_AVATAR = "avatar";
    public static final String USER_PHONE_NUMBER = "phone";
    public static final String USER_NAME = "username";


    private int id;
    private String token;
    private String email;
    private String avatar;
    private String phoneNumber;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ConvertUser() {
    }

    public ConvertUser(int id, String token, String email, String avatar, String phoneNumber, String userName) {
        this.id = id;
        this.token = token;
        this.email = email;
        this.avatar = avatar;
        this.phoneNumber = phoneNumber;
        this.userName = userName;
    }
}
