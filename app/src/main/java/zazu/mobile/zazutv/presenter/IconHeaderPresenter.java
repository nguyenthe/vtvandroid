package zazu.mobile.zazutv.presenter;

import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.demo.Icon;
import zazu.mobile.zazutv.ui.item.IconHeaderCardView;

/**
 * Created by Admin on 11/11/2017.
 */

public class IconHeaderPresenter extends Presenter {
    public IconHeaderPresenter(){}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        HorizontalGridView horizontalGridView = parent.findViewById(R.id.row_content);
        horizontalGridView.setItemSpacing(parent.getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_20));

        return new ViewHolder(new IconHeaderCardView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ((IconHeaderCardView) viewHolder.view).bind((Icon) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }


}
