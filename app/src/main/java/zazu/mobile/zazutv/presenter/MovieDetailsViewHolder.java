package zazu.mobile.zazutv.presenter;

import android.content.Context;
import android.support.v17.leanback.widget.Presenter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

/**
 * Created by Admin on 11/13/2017.
 */

public class MovieDetailsViewHolder extends Presenter.ViewHolder {

    @BindView(R.id.img_Thumb)
    ImageView imgThumb;

    @BindView(R.id.tv_TimeDuration)
    TextView tvTimeDuration;

    @BindView(R.id.tv_Description)
    TextView tvDescription;

    @BindView(R.id.tv_Actor)
    TextView tvActor;

    @BindView(R.id.tv_Director)
    TextView tvDirector;

    @BindView(R.id.tv_TitleFilm)
    TextView tvTitle;

    @BindView(R.id.tv_Type)
    TextView tvType;

    private View itemView;
    private Context context;
    public MovieDetailsViewHolder(View view, Context context) {
        super(view);
        ButterKnife.bind(this, view);
        itemView = view;
        this.context = context;
    }

    public void bind(Datum datum){
        if(datum != null){
            Glide.with(context)
                    .load(((Datum) datum).getBackgroundImage())
                    .centerCrop()
                    .error(R.drawable.details_img)
                    .into(imgThumb);
            tvTitle.setText(datum.getTitle());
            tvDescription.setText(datum.getDescription());
            tvTimeDuration.setText(""+datum.getDurationInSecond());
            tvType.setText(datum.getFilmType());
            //
        }
    }

}
