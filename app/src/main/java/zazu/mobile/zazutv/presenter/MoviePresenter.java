package zazu.mobile.zazutv.presenter;

import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.ui.item.MovieCardView;

/**
 * Created by Admin on 11/11/2017.
 */

public class MoviePresenter extends Presenter {
    public MoviePresenter(){}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        HorizontalGridView horizontalGridView = parent.findViewById(R.id.row_content);
        horizontalGridView.setItemSpacing(parent.getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_20));

        MovieCardView movieCardView = new MovieCardView(parent.getContext());
        movieCardView.setFocusable(true);
        movieCardView.setFocusableInTouchMode(true);
        return new ViewHolder(movieCardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ((MovieCardView) viewHolder.view).bind((Datum) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }


}
