package zazu.mobile.zazutv.presenter;

import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;

import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.ui.item.StringView;

/**
 * Created by Admin on 11/11/2017.
 */

public class StringPresenter extends Presenter {
    public StringPresenter(){}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        StringView stringView = new StringView(parent.getContext());
        stringView.setFocusable(true);
        stringView.setFocusableInTouchMode(true);
        return new ViewHolder(stringView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ((StringView) viewHolder.view).bind((Datum) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }


}
