package zazu.mobile.zazutv.presenter;

/**
 * Created by Admin on 11/13/2017.
 */

import android.os.Bundle;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.VerticalGridPresenter;
import android.util.Log;

import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.CardPresenter;
import zazu.mobile.zazutv.Movie;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;

public class VerticalGridFragment extends android.support.v17.leanback.app.VerticalGridFragment {

    private static final String TAG = VerticalGridFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 4;

    private LinkedHashMap<String, List<Movie>> mVideoLists = null;
    private ArrayObjectAdapter mAdapter;
    private SOService soService;
    private static String KEY = "id";

    public static VerticalGridFragment newInstance(int someInt) {
        VerticalGridFragment myFragment = new VerticalGridFragment();
        Bundle args = new Bundle();
        args.putInt(KEY, someInt);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        soService = ApiUtils.getSOService();
        setupFragment();
        setupEventListeners();
    }

    private void setupFragment() {
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);
        soService.getCategoryByGenre(getArguments().getInt(KEY, 0)).enqueue(new Callback<zazu.mobile.zazutv.model.homepage.theme.Example>() {
            @Override
            public void onResponse(Call<zazu.mobile.zazutv.model.homepage.theme.Example> call, Response<zazu.mobile.zazutv.model.homepage.theme.Example> response) {
                mAdapter = new ArrayObjectAdapter(new CardPresenter());
                for (int i = 0; i < response.body().getData().size(); i++) {
                    Datum movie = response.body().getData().get(i);
                    mAdapter.add(movie);
                    setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<zazu.mobile.zazutv.model.homepage.theme.Example> call, Throwable t) {

            }
        });

    }

    private void setupEventListeners() {
        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Movie) {

            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {
        }
    }
}