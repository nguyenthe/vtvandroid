package zazu.mobile.zazutv.presenter;

import android.support.v17.leanback.widget.Presenter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

/**
 * Created by Admin on 11/13/2017.
 */

public class MovieDetailsDescriptionPresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_fragment,parent,false);
        return new MovieDetailsViewHolder(view,parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        Datum datum = (Datum)item;
        MovieDetailsViewHolder holder = (MovieDetailsViewHolder)viewHolder;
        holder.bind(datum);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }
}
