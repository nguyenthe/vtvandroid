package zazu.mobile.zazutv.model.episodes;

/**
 * Created by Admin on 12/12/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class File {

    @SerializedName("source")
    @Expose
    private Integer source;
    @SerializedName("resolution")
    @Expose
    private Object resolution;
    @SerializedName("url")
    @Expose
    private String url;

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Object getResolution() {
        return resolution;
    }

    public void setResolution(Object resolution) {
        this.resolution = resolution;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}