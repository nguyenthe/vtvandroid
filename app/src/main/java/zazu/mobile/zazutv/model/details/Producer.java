package zazu.mobile.zazutv.model.details;

/**
 * Created by Admin on 11/6/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Producer {

    @SerializedName("artists")
    @Expose
    private List<Artist> artists = null;
    @SerializedName("name")
    @Expose
    private String name;

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
