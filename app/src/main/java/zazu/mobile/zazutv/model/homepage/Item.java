package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 10/2/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("name")
    @Expose
    private String title;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("billboardContext")
    @Expose
    private String billboardContext;
    @SerializedName("contextUrl")
    @Expose
    private String contextUrl;
    @SerializedName("cover_mode")
    @Expose
    private Integer coverMode;
    @SerializedName("cover_mode_display")
    @Expose
    private String coverModeDisplay;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBillboardContext() {
        return billboardContext;
    }

    public void setBillboardContext(String billboardContext) {
        this.billboardContext = billboardContext;
    }

    public String getContextUrl() {
        return contextUrl;
    }

    public void setContextUrl(String contextUrl) {
        this.contextUrl = contextUrl;
    }

    public Integer getCoverMode() {
        return coverMode;
    }

    public void setCoverMode(Integer coverMode) {
        this.coverMode = coverMode;
    }

    public String getCoverModeDisplay() {
        return coverModeDisplay;
    }

    public void setCoverModeDisplay(String coverModeDisplay) {
        this.coverModeDisplay = coverModeDisplay;
    }


}
