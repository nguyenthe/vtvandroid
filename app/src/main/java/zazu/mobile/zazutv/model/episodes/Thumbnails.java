package zazu.mobile.zazutv.model.episodes;

/**
 * Created by Admin on 12/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import zazu.mobile.zazutv.model.homepage.Landscape;


public class Thumbnails {

    @SerializedName("landscape")
    @Expose
    private Landscape landscape;

    public Landscape getLandscape() {
        return landscape;
    }

    public void setLandscape(Landscape landscape) {
        this.landscape = landscape;
    }

}
