package zazu.mobile.zazutv.model.general;

/**
 * Created by Admin on 10/20/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pagination {

    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

}