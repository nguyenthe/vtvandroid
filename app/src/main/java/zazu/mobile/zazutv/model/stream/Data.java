package zazu.mobile.zazutv.model.stream;

/**
 * Created by Admin on 12/7/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("titleId")
    @Expose
    private Integer titleId;
    @SerializedName("seasonId")
    @Expose
    private Integer seasonId;
    @SerializedName("episodeId")
    @Expose
    private Integer episodeId;
    @SerializedName("stream")
    @Expose
    private String stream;

    public Integer getTitleId() {
        return titleId;
    }

    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(Integer episodeId) {
        this.episodeId = episodeId;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

}