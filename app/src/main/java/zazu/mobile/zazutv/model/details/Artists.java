package zazu.mobile.zazutv.model.details;

/**
 * Created by Admin on 10/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import zazu.mobile.zazutv.model.homepage.theme.Actress;

public class Artists {

    @SerializedName("actress")
    @Expose
    private Actress actress;
    @SerializedName("actor")
    @Expose
    private Actor actor;
    @SerializedName("producer")
    @Expose
    private Producer producer;
    @SerializedName("director")
    @Expose
    private Director director;

    public Actress getActress() {
        return actress;
    }

    public void setActress(Actress actress) {
        this.actress = actress;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

}