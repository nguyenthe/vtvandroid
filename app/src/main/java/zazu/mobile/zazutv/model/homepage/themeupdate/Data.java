package zazu.mobile.zazutv.model.homepage.themeupdate;

/**
 * Created by Admin on 11/25/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import zazu.mobile.zazutv.model.homepage.Item;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class Data {

    @SerializedName("items")
    @Expose
    private List<Datum> items = null;
    @SerializedName("objectDetail")
    @Expose
    private Item objectDetail = null;

    public List<Datum> getItems() {
        return items;
    }

    public void setItems(List<Datum> items) {
        this.items = items;
    }

    public Item getObjectDetail() {
        return objectDetail;
    }

    public void setObjectDetail(Item objectDetail) {
        this.objectDetail = objectDetail;
    }
}
