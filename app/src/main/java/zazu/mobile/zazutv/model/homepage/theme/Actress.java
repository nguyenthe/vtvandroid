package zazu.mobile.zazutv.model.homepage.theme;

/**
 * Created by Admin on 10/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Actress implements Serializable {

    @SerializedName("artists")
    @Expose
    private List<Artist__> artists = null;
    @SerializedName("name")
    @Expose
    private String name;

    public List<Artist__> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist__> artists) {
        this.artists = artists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
