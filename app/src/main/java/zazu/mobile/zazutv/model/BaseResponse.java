package zazu.mobile.zazutv.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import zazu.mobile.zazutv.model.homepage.Errors;

/**
 * Created by User on 4/18/2018.
 */

public class BaseResponse<T> {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private T data;
    @SerializedName("errors")
    @Expose
    private Errors errors;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
