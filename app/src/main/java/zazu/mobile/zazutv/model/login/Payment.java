package zazu.mobile.zazutv.model.login;

/**
 * Created by Admin on 10/10/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("current_plan")
    @Expose
    private Object currentPlan;
    @SerializedName("expired_at")
    @Expose
    private Object expiredAt;
    @SerializedName("latest_pay_at")
    @Expose
    private Object latestPayAt;

    public Object getCurrentPlan() {
        return currentPlan;
    }

    public void setCurrentPlan(Object currentPlan) {
        this.currentPlan = currentPlan;
    }

    public Object getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Object expiredAt) {
        this.expiredAt = expiredAt;
    }

    public Object getLatestPayAt() {
        return latestPayAt;
    }

    public void setLatestPayAt(Object latestPayAt) {
        this.latestPayAt = latestPayAt;
    }

}