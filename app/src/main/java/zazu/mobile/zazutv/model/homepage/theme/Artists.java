package zazu.mobile.zazutv.model.homepage.theme;

/**
 * Created by Admin on 10/11/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Artists implements Serializable {

    public enum EnumArtist {
        @SerializedName("director")
        DIRECTOR,
        @SerializedName("cast")
        CAST,
        UNKNOWN
    }

    public class Crew {
        private long id;
        private String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private EnumArtist data_key;
    private List<Crew> crew;

    public EnumArtist getData_key() {
        return data_key;
    }

    public void setData_key(EnumArtist data_key) {
        this.data_key = data_key;
    }

    public List<Crew> getCrew() {
        return crew;
    }

    public void setCrew(List<Crew> crew) {
        this.crew = crew;
    }
}