package zazu.mobile.zazutv.model.details;

/**
 * Created by Admin on 11/6/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Award {

    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("award")
    @Expose
    private Award_ award;
    @SerializedName("awardCategories")
    @Expose
    private List<AwardCategory> awardCategories = null;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Award_ getAward() {
        return award;
    }

    public void setAward(Award_ award) {
        this.award = award;
    }

    public List<AwardCategory> getAwardCategories() {
        return awardCategories;
    }

    public void setAwardCategories(List<AwardCategory> awardCategories) {
        this.awardCategories = awardCategories;
    }

}