package zazu.mobile.zazutv.model.homepage.theme;

/**
 * Created by Admin on 10/11/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaturityRating implements Parcelable {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("minAge")
    @Expose
    private String minAge;

    protected MaturityRating(Parcel in) {
        code = in.readString();
        description = in.readString();
        minAge = in.readString();
    }

    public static final Creator<MaturityRating> CREATOR = new Creator<MaturityRating>() {
        @Override
        public MaturityRating createFromParcel(Parcel in) {
            return new MaturityRating(in);
        }

        @Override
        public MaturityRating[] newArray(int size) {
            return new MaturityRating[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinAge() {
        return minAge;
    }

    public void setMinAge(String minAge) {
        this.minAge = minAge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(description);
        parcel.writeString(minAge);
    }
}
