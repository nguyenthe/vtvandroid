package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 10/2/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("thumb")
    @Expose
    private Integer thumb;
    @SerializedName("added")
    @Expose
    private Boolean added;
    @SerializedName("matched")
    @Expose
    private Integer matched;
    @SerializedName("watching")
    @Expose
    private Object watching;

    public Integer getThumb() {
        return thumb;
    }

    public void setThumb(Integer thumb) {
        this.thumb = thumb;
    }

    public Boolean getAdded() {
        return added;
    }

    public void setAdded(Boolean added) {
        this.added = added;
    }

    public Integer getMatched() {
        return matched;
    }

    public void setMatched(Integer matched) {
        this.matched = matched;
    }

    public Object getWatching() {
        return watching;
    }

    public void setWatching(Object watching) {
        this.watching = watching;
    }

}
