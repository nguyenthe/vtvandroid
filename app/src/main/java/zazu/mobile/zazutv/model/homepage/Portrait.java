package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Portrait implements Parcelable {

    @SerializedName("urls")
    @Expose
    private Urls_ urls;
    @SerializedName("type_display")
    @Expose
    private String typeDisplay;
    @SerializedName("type")
    @Expose
    private Integer type;

    protected Portrait(Parcel in) {
        typeDisplay = in.readString();
        urls = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeDisplay);
        dest.writeParcelable(urls,flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Portrait> CREATOR = new Creator<Portrait>() {
        @Override
        public Portrait createFromParcel(Parcel in) {
            return new Portrait(in);
        }

        @Override
        public Portrait[] newArray(int size) {
            return new Portrait[size];
        }
    };

    public Urls_ getUrls() {
        return urls;
    }

    public void setUrls(Urls_ urls) {
        this.urls = urls;
    }

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}

