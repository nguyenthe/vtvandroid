package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 1/12/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls____ {

    @SerializedName("640_360")
    @Expose
    private String _640360;
    @SerializedName("320_180")
    @Expose
    private String _320180;
    @SerializedName("120_68")
    @Expose
    private String _12068;
    @SerializedName("480_270")
    @Expose
    private String _480270;

    public String get640360() {
        return _640360;
    }

    public void set640360(String _640360) {
        this._640360 = _640360;
    }

    public String get320180() {
        return _320180;
    }

    public void set320180(String _320180) {
        this._320180 = _320180;
    }

    public String get12068() {
        return _12068;
    }

    public void set12068(String _12068) {
        this._12068 = _12068;
    }

    public String get480270() {
        return _480270;
    }

    public void set480270(String _480270) {
        this._480270 = _480270;
    }

}
