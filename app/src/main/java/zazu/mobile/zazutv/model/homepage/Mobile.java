package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 1/12/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mobile {

    @SerializedName("urls")
    @Expose
    private Urls___ urls;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("type_display")
    @Expose
    private String typeDisplay;

    public Urls___ getUrls() {
        return urls;
    }

    public void setUrls(Urls___ urls) {
        this.urls = urls;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

}
