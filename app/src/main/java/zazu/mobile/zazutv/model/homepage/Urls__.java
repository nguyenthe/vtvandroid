package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls__ {

    @SerializedName("342_684")
    @Expose
    private String _342684;
    @SerializedName("540_1080")
    @Expose
    private String _5401080;

    public String get342684() {
        return _342684;
    }

    public void set342684(String _342684) {
        this._342684 = _342684;
    }

    public String get5401080() {
        return _5401080;
    }

    public void set5401080(String _5401080) {
        this._5401080 = _5401080;
    }


}


