package zazu.mobile.zazutv.model.episodes;

/**
 * Created by Admin on 12/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Review {

    @SerializedName("source")
    @Expose
    private Integer source;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("source_display")
    @Expose
    private String sourceDisplay;
    @SerializedName("duration")
    @Expose
    private Integer duration;

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceDisplay() {
        return sourceDisplay;
    }

    public void setSourceDisplay(String sourceDisplay) {
        this.sourceDisplay = sourceDisplay;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

}
