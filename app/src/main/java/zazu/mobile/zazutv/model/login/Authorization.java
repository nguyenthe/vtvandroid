package zazu.mobile.zazutv.model.login;

/**
 * Created by Admin on 10/10/2017.
 */

public class Authorization {

    public Authorization(){}

    public Authorization(String token){
        this.token = token;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
