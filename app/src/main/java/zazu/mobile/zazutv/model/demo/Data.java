package zazu.mobile.zazutv.model.demo;

/**
 * Created by Admin on 9/20/2017.
 */

public class Data {
    private String url;
    private String name_Film;
    private String sum_Minute;
    private String description;

    public Data() {
    }

    public Data(String url, String name_Film, String sum_Minute, String description) {
        this.url = url;
        this.name_Film = name_Film;
        this.sum_Minute = sum_Minute;
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName_Film() {
        return name_Film;
    }

    public void setName_Film(String name_Film) {
        this.name_Film = name_Film;
    }

    public String getSum_Minute() {
        return sum_Minute;
    }

    public void setSum_Minute(String sum_Minute) {
        this.sum_Minute = sum_Minute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Data{" +
                "url='" + url + '\'' +
                ", name_Film='" + name_Film + '\'' +
                ", sum_Minute='" + sum_Minute + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
