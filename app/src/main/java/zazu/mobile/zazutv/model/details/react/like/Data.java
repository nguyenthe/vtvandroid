package zazu.mobile.zazutv.model.details.react.like;

/**
 * Created by Admin on 10/25/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("titleId")
    @Expose
    private Integer titleId;
    @SerializedName("newRating")
    @Expose
    private Integer newRating;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getTitleId() {
        return titleId;
    }

    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    public Integer getNewRating() {
        return newRating;
    }

    public void setNewRating(Integer newRating) {
        this.newRating = newRating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
