package zazu.mobile.zazutv.model.homepage.theme;

/**
 * Created by Admin on 10/2/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import zazu.mobile.zazutv.model.homepage.Artist;
import zazu.mobile.zazutv.model.homepage.Covers;
import zazu.mobile.zazutv.model.homepage.Crew;
import zazu.mobile.zazutv.model.homepage.Season;

public class Datum implements Parcelable {
    //static final long serialVersionUID = 727566175075960653L;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("titleLogo")
    @Expose
    private String titleLogo;
    @SerializedName("backgroundImage")
    @Expose
    private String backgroundImage;
    @SerializedName("thumbnailImage")
    @Expose
    private String thumbnailImage;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("watchLink")
    @Expose
    private String watchLink;
    @SerializedName("catalogLink")
    @Expose
    private String catalogLink;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("maturityRating")
    @Expose
    private MaturityRating maturityRating;
    @SerializedName("filmType")
    @Expose
    private String filmType;
    @SerializedName("matchScore")
    @Expose
    private String matchScore;
    @SerializedName("durationInSecond")
    @Expose
    private int durationInSecond;
    @SerializedName("episodeIndex")
    @Expose
    private int episodeIndex;
    @SerializedName("durationText")
    @Expose
    private String durationText;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = null;
    @SerializedName("artists")
    @Expose
    private List<zazu.mobile.zazutv.model.homepage.Artist> artists;
    @SerializedName("genres")
    @Expose
    private List<Genre> genres = new ArrayList<Genre>();
    @SerializedName("highestResolution")
    @Expose
    private String highestResolution;
    @SerializedName("stream")
    @Expose
    private String stream;
    @SerializedName("awards")
    @Expose
    private List<Object> awards = null;
    @SerializedName("covers")
    @Expose
    private Covers covers;
    @SerializedName("seasons")
    @Expose
    private List<Season> seasons = new ArrayList<Season>();
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("type_display")
    @Expose
    private String typeDisplay;

    @SerializedName("seasonId")
    @Expose
    private int seasonId;
    @SerializedName("episodeId")
    @Expose
    private int episodeId;


    protected Datum(Parcel in) {
        id = in.readInt();
        title = in.readString();
        titleLogo = in.readString();
        backgroundImage = in.readString();
        thumbnailImage = in.readString();
        link = in.readString();
        watchLink = in.readString();
        catalogLink = in.readString();
        description = in.readString();
        year = in.readString();
        maturityRating = in.readParcelable(MaturityRating.class.getClassLoader());
        filmType = in.readString();
        matchScore = in.readString();
        durationText = in.readString();
        artists = in.createTypedArrayList(zazu.mobile.zazutv.model.homepage.Artist.CREATOR);
        genres = in.createTypedArrayList(Genre.CREATOR);
        highestResolution = in.readString();
        stream = in.readString();
        covers = in.readParcelable(Covers.class.getClassLoader());
        seasons = in.createTypedArrayList(Season.CREATOR);
        typeDisplay = in.readString();
        type = in.readInt();
        seasonId = in.readInt();
        episodeId = in.readInt();
        episodeIndex = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(titleLogo);
        dest.writeString(backgroundImage);
        dest.writeString(thumbnailImage);
        dest.writeString(link);
        dest.writeString(watchLink);
        dest.writeString(catalogLink);
        dest.writeString(description);
        dest.writeString(year);
        dest.writeParcelable(maturityRating, flags);
        dest.writeString(filmType);
        dest.writeString(matchScore);
        dest.writeString(durationText);
        dest.writeTypedList(artists);
        dest.writeTypedList(genres);
        dest.writeString(highestResolution);
        dest.writeString(stream);
        dest.writeParcelable(covers, flags);
        dest.writeTypedList(seasons);
        dest.writeString(typeDisplay);
        dest.writeInt(type);
        dest.writeInt(seasonId);
        dest.writeInt(episodeId);
        dest.writeInt(episodeIndex);
    }

    public static final Creator<Datum> CREATOR = new Creator<Datum>() {
        @Override
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        @Override
        public Datum[] newArray(int size) {
            return new Datum[size];
        }
    };

    public int getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleLogo() {
        return titleLogo;
    }

    public void setTitleLogo(String titleLogo) {
        this.titleLogo = titleLogo;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getWatchLink() {
        return watchLink;
    }

    public void setWatchLink(String watchLink) {
        this.watchLink = watchLink;
    }

    public String getCatalogLink() {
        return catalogLink;
    }

    public void setCatalogLink(String catalogLink) {
        this.catalogLink = catalogLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public MaturityRating getMaturityRating() {
        return maturityRating;
    }

    public void setMaturityRating(MaturityRating maturityRating) {
        this.maturityRating = maturityRating;
    }

    public String getFilmType() {
        return filmType;
    }

    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }

    public int getDurationInSecond() {
        return durationInSecond;
    }

    public void setDurationInSecond(int durationInSecond) {
        this.durationInSecond = durationInSecond;
    }

    public int getEpisodeIndex() {
        return episodeIndex;
    }

    public void setEpisodeIndex(int episodeIndex) {
        this.episodeIndex = episodeIndex;
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<zazu.mobile.zazutv.model.homepage.Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getHighestResolution() {
        return highestResolution;
    }

    public void setHighestResolution(String highestResolution) {
        this.highestResolution = highestResolution;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public List<Object> getAwards() {
        return awards;
    }

    public void setAwards(List<Object> awards) {
        this.awards = awards;
    }

    public Covers getCovers() {
        return covers;
    }

    public void setCovers(Covers covers) {
        this.covers = covers;
    }

    public List<Season> getSeasons() {
        if (seasons != null) {
            Collections.sort(seasons, new Comparator<Season>() {
                @Override
                public int compare(Season season, Season t1) {
                    return season.getIndex().compareTo(t1.getIndex());
                }
            });
        }
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Datum() {
    }

    public String getDirectors() {
        StringBuilder director = new StringBuilder();
        if (getArtists() != null) {
            List<Artist> artist = getArtists();
            for (int i = 0; i < artist.size(); i++) {
                Artist artist1 = artist.get(i);
                if (artist1.getCategory().equalsIgnoreCase("director")) {
                    if (artist1.getCrew() != null) {
                        List<Crew> crewsList = artist1.getCrew();
                        int size = crewsList.size();
                        for (int j = 0; j < size; j++) {
                            director.append(crewsList.get(j).getName()).append(", ");
                        }
                        if (size > 0)
                            director = new StringBuilder(director.substring(0, director.length() - 1));
                    }
                }
            }
        }
        return director.toString();
    }

    public String getActors() {
        StringBuilder actor = new StringBuilder();
        if (getArtists() != null) {
            List<Artist> artist = getArtists();
            for (int i = 0; i < artist.size(); i++) {
                Artist artist1 = artist.get(i);
                if (artist1.getCategory().equalsIgnoreCase("cast")) {
                    if (artist1.getCrew() != null) {
                        List<Crew> crewsList = artist1.getCrew();
                        int size = crewsList.size();
                        for (int j = 0; j < size; j++) {
                            actor.append(crewsList.get(j).getName()).append(", ");
                        }
                        if (size > 0)
                            actor = new StringBuilder(actor.substring(0, actor.length() - 2));
                    }
                }
            }
        }
        return actor.toString();
    }

    public String getGenresText() {
        StringBuilder genres = new StringBuilder();
        if (getGenres() != null) {
            List<Genre> genreList = getGenres();
            for (int i = 0; i < genreList.size(); i++) {
                genres.append(genreList.get(i).getName()).append(", ");

            }
            if (genreList.size() > 0)
                genres = new StringBuilder(genres.substring(0, genres.length() - 2));
        }
        return genres.toString();
    }

}