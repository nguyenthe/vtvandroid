package zazu.mobile.zazutv.model.homepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 10/2/2017.
 */

public class Errors {
    @SerializedName("billboardOriginal")
    @Expose
    private BillboardOriginal billboardOriginal;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public BillboardOriginal getBillboardOriginal() {
        return billboardOriginal;
    }

    public void setBillboardOriginal(BillboardOriginal billboardOriginal) {
        this.billboardOriginal = billboardOriginal;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
