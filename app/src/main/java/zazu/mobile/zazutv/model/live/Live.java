package zazu.mobile.zazutv.model.live;

/**
 * Created by Admin on 10/20/2017.
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Live implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("mobile_logo")
    @Expose
    private String mobile_logo;
    @SerializedName("seo_title")
    @Expose
    private String seo_title;
    @SerializedName("channel_id")
    @Expose
    private int channel_id;

    private String url;

    public static final Creator<Live> CREATOR = new Creator<Live>() {
        @Override
        public Live createFromParcel(Parcel in) {
            return new Live(in);
        }

        @Override
        public Live[] newArray(int size) {
            return new Live[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getMobile_logo() {
        return mobile_logo;
    }

    public void setMobile_logo(String mobile_logo) {
        this.mobile_logo = mobile_logo;
    }

    public String getSeo_title() {
        return seo_title;
    }

    public void setSeo_title(String seo_title) {
        this.seo_title = seo_title;
    }

    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected Live(Parcel in) {
        id = in.readInt();
        name = in.readString();
        slug = in.readString();
        isActive = in.readInt() == 1;
        mobile_logo = in.readString();
        seo_title = in.readString();
        channel_id = in.readInt();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(slug);
        dest.writeInt(isActive ? 1 : 0);
        dest.writeString(mobile_logo);
        dest.writeString(seo_title);
        dest.writeInt(channel_id);
        dest.writeString(url);
    }

    public String getUrl() {
        return url;
    }
}
