package zazu.mobile.zazutv.model.details;

/**
 * Created by Admin on 10/16/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class Data {

    @SerializedName("item")
    @Expose
    private Datum item;

    public Datum getItem() {
        return item;
    }

    public void setItem(Datum item) {
        this.item = item;
    }

}