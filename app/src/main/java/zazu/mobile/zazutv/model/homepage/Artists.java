package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 10/2/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artists {

    /*@SerializedName("actor")
    @Expose
    private Actor actor;
    @SerializedName("director")
    @Expose
    private Director director;
    @SerializedName("actress")
    @Expose
    private Actress actress;

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Actress getActress() {
        return actress;
    }

    public void setActress(Actress actress) {
        this.actress = actress;
    }*/
    @SerializedName("cast")
    @Expose
    private Cast cast;
    @SerializedName("director")
    @Expose
    private Director director;

    public Cast getCast() {
        return cast;
    }

    public void setCast(Cast cast) {
        this.cast = cast;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }


}