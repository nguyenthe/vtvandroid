package zazu.mobile.zazutv.model.header;

/**
 * Created by Admin on 11/9/2017.
 */

public class HeaderItem extends android.support.v17.leanback.widget.HeaderItem {

    private static final String TAG = HeaderItem.class.getSimpleName();
    public static final int ICON_NONE = -1;

    /** Hold an icon resource id */

    public HeaderItem(String name) {
        super(name);
    }

    public static String getTAG() {
        return TAG;
    }

    public HeaderItem(long id, String name) {
        super(id, name);
    }
}
