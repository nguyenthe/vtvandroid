package zazu.mobile.zazutv.model.login;

/**
 * Created by Admin on 12/5/2017.
 */

public class LoginDB {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginDB() {
    }

    public LoginDB(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
