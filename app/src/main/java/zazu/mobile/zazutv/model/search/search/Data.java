package zazu.mobile.zazutv.model.search.search;

/**
 * Created by Admin on 10/31/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import zazu.mobile.zazutv.model.general.Pagination;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class Data {

    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("titles")
    @Expose
    private List<Datum> titles = null;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Datum> getTitles() {
        return titles;
    }

    public void setTitles(List<Datum> titles) {
        this.titles = titles;
    }

}
