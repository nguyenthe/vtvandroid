package zazu.mobile.zazutv.model.homepage.mylist.detailmylist;

/**
 * Created by Admin on 10/19/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class Data {

    @SerializedName("titles")
    @Expose
    private List<Datum> items = null;

    public List<Datum> getItems() {
        return items;
    }

    public void setItems(List<Datum> items) {
        this.items = items;
    }

}
