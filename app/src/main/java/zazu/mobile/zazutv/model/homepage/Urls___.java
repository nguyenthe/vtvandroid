package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 1/12/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls___ {

    @SerializedName("440_640")
    @Expose
    private String _440640;
    @SerializedName("220_320")
    @Expose
    private String _220320;
    @SerializedName("110_160")
    @Expose
    private String _110160;
    @SerializedName("330_480")
    @Expose
    private String _330480;

    public String get440640() {
        return _440640;
    }

    public void set440640(String _440640) {
        this._440640 = _440640;
    }

    public String get220320() {
        return _220320;
    }

    public void set220320(String _220320) {
        this._220320 = _220320;
    }

    public String get110160() {
        return _110160;
    }

    public void set110160(String _110160) {
        this._110160 = _110160;
    }

    public String get330480() {
        return _330480;
    }

    public void set330480(String _330480) {
        this._330480 = _330480;
    }

}
