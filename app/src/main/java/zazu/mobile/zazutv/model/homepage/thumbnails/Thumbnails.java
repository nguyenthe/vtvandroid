package zazu.mobile.zazutv.model.homepage.thumbnails;

/**
 * Created by Admin on 10/2/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Thumbnails implements Serializable {

    @SerializedName("1920_1080")
    @Expose
    private _19201080 _19201080;
    @SerializedName("480_270")
    @Expose
    private _480270 _480270;
    @SerializedName("120_68")
    @Expose
    private _12068 _12068;
    @SerializedName("320_180")
    @Expose
    private _320180 _320180;
    @SerializedName("640_360")
    @Expose
    private _640360 _640360;
    @SerializedName("1280_720")
    @Expose
    private _1280720 _1280720;
    @SerializedName("980_552")
    @Expose
    private _980552 _980552;

    public _19201080 get19201080() {
        return _19201080;
    }

    public void set19201080(_19201080 _19201080) {
        this._19201080 = _19201080;
    }

    public _480270 get480270() {
        return _480270;
    }

    public void set480270(_480270 _480270) {
        this._480270 = _480270;
    }

    public _12068 get12068() {
        return _12068;
    }

    public void set12068(_12068 _12068) {
        this._12068 = _12068;
    }

    public _320180 get320180() {
        return _320180;
    }

    public void set320180(_320180 _320180) {
        this._320180 = _320180;
    }

    public _640360 get640360() {
        return _640360;
    }

    public void set640360(_640360 _640360) {
        this._640360 = _640360;
    }

    public _1280720 get1280720() {
        return _1280720;
    }

    public void set1280720(_1280720 _1280720) {
        this._1280720 = _1280720;
    }

    public _980552 get980552() {
        return _980552;
    }

    public void set980552(_980552 _980552) {
        this._980552 = _980552;
    }

}
