package zazu.mobile.zazutv.model.details;

/**
 * Created by Admin on 10/11/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Director {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("artists")
    @Expose
    private List<Artist> artists = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

}
