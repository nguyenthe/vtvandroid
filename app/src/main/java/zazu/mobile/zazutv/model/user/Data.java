package zazu.mobile.zazutv.model.user;

/**
 * Created by Admin on 10/24/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("latest_pay_at")
    @Expose
    private Object latestPayAt;
    @SerializedName("expired_at")
    @Expose
    private String expiredAt;
    @SerializedName("current_plan")
    @Expose
    private Integer currentPlan;
    @SerializedName("is_expired")
    @Expose
    private Boolean isExpired;

    public Object getLatestPayAt() {
        return latestPayAt;
    }

    public void setLatestPayAt(Object latestPayAt) {
        this.latestPayAt = latestPayAt;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }

    public Integer getCurrentPlan() {
        return currentPlan;
    }

    public void setCurrentPlan(Integer currentPlan) {
        this.currentPlan = currentPlan;
    }

    public Boolean getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(Boolean isExpired) {
        this.isExpired = isExpired;
    }

}
