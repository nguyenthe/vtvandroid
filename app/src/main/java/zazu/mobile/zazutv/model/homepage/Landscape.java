package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Landscape implements Parcelable {

    @SerializedName("urls")
    @Expose
    private Urls urls;
    @SerializedName("type_display")
    @Expose
    private String typeDisplay;
    @SerializedName("type")
    @Expose
    private Integer type;

    protected Landscape(Parcel in) {
        typeDisplay = in.readString();
        urls = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeDisplay);
        dest.writeParcelable(urls,flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Landscape> CREATOR = new Creator<Landscape>() {
        @Override
        public Landscape createFromParcel(Parcel in) {
            return new Landscape(in);
        }

        @Override
        public Landscape[] newArray(int size) {
            return new Landscape[size];
        }
    };

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}

