package zazu.mobile.zazutv.model.db;

/**
 * Created by Admin on 10/31/2017.
 */

public class Intro {
    private Integer status;
    public Intro(){}

    public Intro(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
