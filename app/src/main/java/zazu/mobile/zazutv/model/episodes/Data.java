package zazu.mobile.zazutv.model.episodes;

/**
 * Created by Admin on 12/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private Object name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("is_multiple_episode")
    @Expose
    private Boolean isMultipleEpisode;
    @SerializedName("episodes")
    @Expose
    private List<Episode> episodes = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsMultipleEpisode() {
        return isMultipleEpisode;
    }

    public void setIsMultipleEpisode(Boolean isMultipleEpisode) {
        this.isMultipleEpisode = isMultipleEpisode;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

}