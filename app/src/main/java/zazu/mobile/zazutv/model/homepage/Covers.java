package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Covers implements Parcelable {

    @SerializedName("landscape")
    @Expose
    private Landscape landscape;
    @SerializedName("portrait")
    @Expose
    private Portrait portrait;

    protected Covers(Parcel in) {
        landscape = in.readParcelable(getClass().getClassLoader());
        portrait = in.readParcelable(getClass().getClassLoader());;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(landscape, flags);
        parcel.writeParcelable(portrait, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Covers> CREATOR = new Creator<Covers>() {
        @Override
        public Covers createFromParcel(Parcel in) {
            return new Covers(in);
        }

        @Override
        public Covers[] newArray(int size) {
            return new Covers[size];
        }
    };

    public Landscape getLandscape() {
        return landscape;
    }

    public void setLandscape(Landscape landscape) {
        this.landscape = landscape;
    }

    public Portrait getPortrait() {
        return portrait;
    }

    public void setPortrait(Portrait portrait) {
        this.portrait = portrait;
    }

}
