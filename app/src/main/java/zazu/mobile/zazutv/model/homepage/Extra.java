package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Extra {

    @SerializedName("type_display")
    @Expose
    private String typeDisplay;
    @SerializedName("urls")
    @Expose
    private Urls__ urls;
    @SerializedName("type")
    @Expose
    private Integer type;

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    public Urls__ getUrls() {
        return urls;
    }

    public void setUrls(Urls__ urls) {
        this.urls = urls;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}