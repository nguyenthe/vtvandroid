package zazu.mobile.zazutv.model.demo;

/**
 * Created by Admin on 9/18/2017.
 */

public class Video {
    private String url;

    public Video(){}

    public Video(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
