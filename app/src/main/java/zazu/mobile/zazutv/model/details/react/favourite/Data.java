package zazu.mobile.zazutv.model.details.react.favourite;

/**
 * Created by Admin on 10/17/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("titleId")
    @Expose
    private Integer titleId;
    @SerializedName("newStatus")
    @Expose
    private Boolean newStatus;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getTitleId() {
        return titleId;
    }

    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    public Boolean getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Boolean newStatus) {
        this.newStatus = newStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
