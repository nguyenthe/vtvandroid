package zazu.mobile.zazutv.model.demo;

/**
 * Created by Admin on 11/14/2017.
 */

public class Icon {
    private int idSource;
    private int idSourceWhite;
    private String title;

    public int getIdSourceWhite() {
        return idSourceWhite;
    }

    public int getIdSource() {
        return idSource;
    }

    public void setIdSource(int idSource) {
        this.idSource = idSource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Icon(int idSource,int idSourceWhite, String title) {
        this.idSource = idSource;
        this.idSourceWhite = idSourceWhite;
        this.title = title;
    }
}
