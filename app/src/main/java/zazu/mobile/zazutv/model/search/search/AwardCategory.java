package zazu.mobile.zazutv.model.search.search;

/**
 * Created by Admin on 11/2/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AwardCategory {

    @SerializedName("awardCategory")
    @Expose
    private AwardCategory_ awardCategory;
    @SerializedName("artists")
    @Expose
    private List<Object> artists = null;

    public AwardCategory_ getAwardCategory() {
        return awardCategory;
    }

    public void setAwardCategory(AwardCategory_ awardCategory) {
        this.awardCategory = awardCategory;
    }

    public List<Object> getArtists() {
        return artists;
    }

    public void setArtists(List<Object> artists) {
        this.artists = artists;
    }

}

