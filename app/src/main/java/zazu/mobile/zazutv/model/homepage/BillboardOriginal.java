package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 10/2/2017.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillboardOriginal {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("titleLogo")
    @Expose
    private String titleLogo;
    @SerializedName("backgroundImage")
    @Expose
    private String backgroundImage;
    @SerializedName("thumbnailImage")
    @Expose
    private String thumbnailImage;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("watchLink")
    @Expose
    private String watchLink;
    @SerializedName("seasonId")
    @Expose
    private Integer seasonId;
    @SerializedName("episodeId")
    @Expose
    private Integer episodeId;
    @SerializedName("catalogLink")
    @Expose
    private String catalogLink;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("maturityRating")
    @Expose
    private MaturityRating maturityRating;
    @SerializedName("matchScore")
    @Expose
    private String matchScore;
    @SerializedName("durationInSecond")
    @Expose
    private Integer durationInSecond;
    @SerializedName("durationText")
    @Expose
    private String durationText;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = null;
    @SerializedName("artists")
    @Expose
    private List<Artist> artists = null;
    @SerializedName("genres")
    @Expose
    private List<Genre> genres = null;
    @SerializedName("highestResolution")
    @Expose
    private String highestResolution;
    @SerializedName("stream")
    @Expose
    private String stream;
    @SerializedName("awards")
    @Expose
    private List<Object> awards = null;
    @SerializedName("covers")
    @Expose
    private Covers covers;
    @SerializedName("seasons")
    @Expose
    private List<Season> seasons = null;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("type_display")
    @Expose
    private String typeDisplay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleLogo() {
        return titleLogo;
    }

    public void setTitleLogo(String titleLogo) {
        this.titleLogo = titleLogo;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getWatchLink() {
        return watchLink;
    }

    public void setWatchLink(String watchLink) {
        this.watchLink = watchLink;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(Integer episodeId) {
        this.episodeId = episodeId;
    }

    public String getCatalogLink() {
        return catalogLink;
    }

    public void setCatalogLink(String catalogLink) {
        this.catalogLink = catalogLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public MaturityRating getMaturityRating() {
        return maturityRating;
    }

    public void setMaturityRating(MaturityRating maturityRating) {
        this.maturityRating = maturityRating;
    }

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }

    public Integer getDurationInSecond() {
        return durationInSecond;
    }

    public void setDurationInSecond(Integer durationInSecond) {
        this.durationInSecond = durationInSecond;
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getHighestResolution() {
        return highestResolution;
    }

    public void setHighestResolution(String highestResolution) {
        this.highestResolution = highestResolution;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public List<Object> getAwards() {
        return awards;
    }

    public void setAwards(List<Object> awards) {
        this.awards = awards;
    }

    public Covers getCovers() {
        return covers;
    }

    public void setCovers(Covers covers) {
        this.covers = covers;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(String typeDisplay) {
        this.typeDisplay = typeDisplay;
    }


}
