package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls implements Parcelable {

    @SerializedName("1920_1080")
    @Expose
    private String _19201080;
    @SerializedName("320_180")
    @Expose
    private String _320180;
    @SerializedName("480_270")
    @Expose
    private String _480270;
    @SerializedName("980_552")
    @Expose
    private String _980552;
    @SerializedName("120_68")
    @Expose
    private String _12068;
    @SerializedName("640_360")
    @Expose
    private String _640360;
    @SerializedName("1280_720")
    @Expose
    private String _1280720;

    protected Urls(Parcel in) {
        _19201080 = in.readString();
        _320180 = in.readString();
        _480270 = in.readString();
        _980552 = in.readString();
        _12068 = in.readString();
        _640360 = in.readString();
        _1280720 = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_19201080);
        dest.writeString(_320180);
        dest.writeString(_480270);
        dest.writeString(_980552);
        dest.writeString(_12068);
        dest.writeString(_640360);
        dest.writeString(_1280720);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Urls> CREATOR = new Creator<Urls>() {
        @Override
        public Urls createFromParcel(Parcel in) {
            return new Urls(in);
        }

        @Override
        public Urls[] newArray(int size) {
            return new Urls[size];
        }
    };

    public String get19201080() {
        return _19201080;
    }

    public void set19201080(String _19201080) {
        this._19201080 = _19201080;
    }

    public String get320180() {
        return _320180;
    }

    public void set320180(String _320180) {
        this._320180 = _320180;
    }

    public String get480270() {
        return _480270;
    }

    public void set480270(String _480270) {
        this._480270 = _480270;
    }

    public String get980552() {
        return _980552;
    }

    public void set980552(String _980552) {
        this._980552 = _980552;
    }

    public String get12068() {
        return _12068;
    }

    public void set12068(String _12068) {
        this._12068 = _12068;
    }

    public String get640360() {
        return _640360;
    }

    public void set640360(String _640360) {
        this._640360 = _640360;
    }

    public String get1280720() {
        return _1280720;
    }

    public void set1280720(String _1280720) {
        this._1280720 = _1280720;
    }

}