package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 10/2/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class Data implements Parcelable, Serializable {

    @SerializedName("billboardOriginal")
    @Expose
    private Datum billboardOriginal;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    protected Data(Parcel in) {
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public Datum getBillboardOriginal() {
        return billboardOriginal;
    }

    public void setBillboardOriginal(Datum billboardOriginal) {
        this.billboardOriginal = billboardOriginal;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}

