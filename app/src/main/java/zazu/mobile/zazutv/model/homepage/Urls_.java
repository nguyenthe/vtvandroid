package zazu.mobile.zazutv.model.homepage;

/**
 * Created by Admin on 11/25/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls_ implements Parcelable{

    @SerializedName("342_684")
    @Expose
    private String _342684;
    @SerializedName("540_1080")
    @Expose
    private String _5401080;

    protected Urls_(Parcel in) {
        _342684 = in.readString();
        _5401080 = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_342684);
        dest.writeString(_5401080);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Urls_> CREATOR = new Creator<Urls_>() {
        @Override
        public Urls_ createFromParcel(Parcel in) {
            return new Urls_(in);
        }

        @Override
        public Urls_[] newArray(int size) {
            return new Urls_[size];
        }
    };

    public String get342684() {
        return _342684;
    }

    public void set342684(String _342684) {
        this._342684 = _342684;
    }

    public String get5401080() {
        return _5401080;
    }

    public void set5401080(String _5401080) {
        this._5401080 = _5401080;
    }

}