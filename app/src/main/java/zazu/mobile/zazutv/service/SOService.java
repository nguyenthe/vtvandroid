package zazu.mobile.zazutv.service;

/**
 * Created by HongNgoc on 5/14/2017.
 */

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import zazu.mobile.zazutv.model.details.react.favourite.Action;
import zazu.mobile.zazutv.model.login.Example;
import zazu.mobile.zazutv.model.login.UserRequest;

public interface SOService {

    @FormUrlEncoded
    @POST("/api/v1/users/auth?getToken=1")
    Call<Example> getToken(@Field("email") String email, @Field("password") String password);

    @POST("/api/v1/users/auth?getToken=1")
    Call<Example> getToken1(@Header("Content-Type") String content_type, @Body UserRequest userRequest);

    @GET("/api/v1/browse/detail")
    Call<zazu.mobile.zazutv.model.homepage.Example> getDataHomePage(); //@Header("Authorization") String token

    @GET("/api/v1/browse")
    Call<zazu.mobile.zazutv.model.homepage.Example> getDataHomePageError(); //@Header("Authorization") String token

    //
    @GET("/api/v1/theme/{themeId}/titles") // browse replace theme and context replace themeId
    Call<zazu.mobile.zazutv.model.homepage.themeupdate.Example> getListFromBlock(@Path("themeId") String theme);//@Header("Authorization") String token,

    @GET("/api/v1/title/{titleId}")
    Call<zazu.mobile.zazutv.model.details.Example> getDetailFilm(@Path("titleId") int theme); //@Header("Authorization") String token, @Header("X-Api-Version") String id,

    @GET("/api/v1/genre")
    Call<zazu.mobile.zazutv.model.menuleft.Example> getMenuLeft(@Query("mainmenu") int content); //@Header("Authorization") String token

    @GET("/api/v1/menu/movie")
    Call<zazu.mobile.zazutv.model.menuleft.Example> getMenuMovie(); //@Header("Authorization") String token

    @GET("/api/v1/menu/tv-show")
    Call<zazu.mobile.zazutv.model.menuleft.Example> getMenuTvShow(); //@Header("Authorization") String token

    @GET("/api/v1/menu/tv-show")
    Call<zazu.mobile.zazutv.model.menuleft.Example> getLiveChannel(); //@Header("Authorization") String token

    @GET("/api/v1/browse/genre:{GENRE_ID}/titles")
    Call<zazu.mobile.zazutv.model.homepage.theme.Example>
    getCategoryByGenre(@Path("GENRE_ID") int id); //@Header("Authorization") String token,

    @GET("/api/v1/title/genre/{slug}")
    Call<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example>
    getMoviesByGenre(@Path("slug") String slug);

    @GET("/api/v1/title/genre/{slug}")
    Call<zazu.mobile.zazutv.model.search.filter.Example>
    getMoviesByGenreId(@Path("slug") String slug, @Query("page") int page,
                       @Query("size") int size);

    @GET("/api/v1/channel")
    Call<zazu.mobile.zazutv.model.live.Example>
    getChannels();

    @POST("/api/v1/title/{titleId}/favourite")
    Call<zazu.mobile.zazutv.model.details.react.favourite.Example> postFavourite(@Body Action action, @Header("Authorization") String token); //@Header("Authorization") String token,

    @POST("/api/v1/title/{titleId}/thumb")
    Call<zazu.mobile.zazutv.model.details.react.favourite.Example> postLike(@Header("Authorization")String token, @Body Action action);//@Header("Authorization")String token,

    @GET("/api/v1/my-list")
    Call<zazu.mobile.zazutv.model.homepage.mylist.Example> getMyList(@Header("Authorization") String token); //@Header("Authorization") String token

    @GET("/api/v1/my-list/titles")
    Call<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example> getMyListDetail(@Header("Authorization") String token); //@Header("Authorization") String token

    @GET("/api/v1/title/genre/{GENRE_ID}?page=1&size=10")
    Call<zazu.mobile.zazutv.model.search.filter.Example> getListFilterByGenre(@Path("GENRE_ID") String id);//@Header("Authorization") String token,

    @GET("/api/v1/payment")
    Call<zazu.mobile.zazutv.model.user.Example> getPaymentInfo();// @Header("Authorization") String token

    @GET("/api/v1/title/{titleId}/watch")
    Call<zazu.mobile.zazutv.model.details.stream.Example> getStream(@Header("Authorization") String token, @Path("titleId") int titleId);

    //search
    @GET("/api/v1/title")
    Call<zazu.mobile.zazutv.model.search.search.Example> getDataSearch(@Query("query") String content,
                                                                       @Query("page") int page,
                                                                       @Query("size") int size
    );

    @GET("/api/v1/video")
    Call<Example> getResultSearch(@Query("query") String content,
                                                                        @Query("page") int page,
                                                                        @Query("size") int size
    );

    @GET("/api/v1/title/{titleId}/season/{seasonId}/episode/{episodeId}/watch")
    Call<zazu.mobile.zazutv.model.stream.Example>
    getStreamLinkMovie(@Path("titleId") int titleId, @Path("seasonId") int seasonId,
                       @Path("episodeId") int episodeId);

    //get episode by session
    @GET("/api/v1/title/{titleId}/season/{seasonId}")
    Call<zazu.mobile.zazutv.model.episodes.Example> getEpisodesBySession(
            @Path("titleId") int titleId, @Path("seasonId") int seasonId);

    @GET("/api/v1/title/{titleId}/similar")
    Call<zazu.mobile.zazutv.model.search.search.Example> getSimilar(
            @Path("titleId") int titleId);

}
