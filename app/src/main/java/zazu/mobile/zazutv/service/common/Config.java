package zazu.mobile.zazutv.service.common;

/**
 * Created by Admin on 10/16/2017.
 */

public class Config {
    public static String idDetailFilm = "idDetailFilm";
    public static String idTheme = "idTheme";
    public static String idToken = "Bearer ";
    public static String idAddFavorite = "add";
    public static String idRemoveFavorite = "remove";
    public static String URL = "https://beta.zazu.vn/";
    public static String FLAG="It the start";
    public static String ERROR_TRY_CASH = " Error crash ";
    public static String nameFilm = "nameFilm";
    public static String idFilm = "idFilm";
    public static final String DATUM = "Datum";
    public static final String LIVE = "Live";
    public static final String TITLE = "title";

}
