package zazu.mobile.zazutv.service;

/**
 * Created by HongNgoc on 5/14/2017.
 */

public class ApiUtils {

    //public static final String BASE_URL = "https://beta.zazu.vn/";
//    public static final String BASE_URL_1 = "https://demo.zazu.vn/";
    public static final String BASE_URL = "https://www.vtvgiaitri.vn/api/v1/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }

}
