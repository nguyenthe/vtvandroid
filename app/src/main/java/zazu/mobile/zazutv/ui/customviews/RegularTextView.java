package zazu.mobile.zazutv.ui.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

/**
 * Created by ZINNO on 4/13/2018.
 */

public class RegularTextView extends AppCompatTextView {


  public RegularTextView(Context context) {
    super(context);
    init(context);
  }

  public RegularTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public RegularTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  private void init(Context context){
    if(!isInEditMode()){
      Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
      this.setTypeface(face);
    }
  }

}
