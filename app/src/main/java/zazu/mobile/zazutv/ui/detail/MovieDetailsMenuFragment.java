package zazu.mobile.zazutv.ui.detail;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.owen.tvrecyclerview.TwoWayLayoutManager;
import com.owen.tvrecyclerview.widget.ListLayoutManager;
import com.owen.tvrecyclerview.widget.TvRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.SeasonAdapter;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.ui.dialog.episodes.EpisodesFilmNew;
import zazu.mobile.zazutv.ui.item.HorizontalSpaceItemDecoration;

/**
 * Created by Admin on 11/13/2017.
 */

public class MovieDetailsMenuFragment extends Fragment {

    public static String TAG = "MovieDetails-Fragment";
    public static String TRANSITION_NAME = "poster_transition";
    private Datum datum = null;
    private int currentPosition = -1;

    @BindView(R.id.rv_seasons)
    TvRecyclerView rv_seasons;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    public static MovieDetailsMenuFragment newInstance(Datum movie) {
        Bundle args = new Bundle();
        args.putParcelable(Config.DATUM, movie);
        MovieDetailsMenuFragment fragment = new MovieDetailsMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datum = getArguments().getParcelable(Config.DATUM);
        Log.e(TAG, " object : " + datum.getBackgroundImage());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_menu_fragment, container, false);
        ButterKnife.bind(this, view);

        initView();
        return view;
    }

    private void initView() {
        ListLayoutManager manager = new ListLayoutManager(getActivity(), TwoWayLayoutManager.Orientation.HORIZONTAL);
        rv_seasons.setLayoutManager(manager);

        SeasonAdapter adapter = new SeasonAdapter(getActivity(), datum);
        adapter.setOnItemStateListener(new SeasonAdapter.OnItemStateListener() {
            @Override
            public void onItemClick(View view, int position) {
                setPosition(position);
            }
        });
        rv_seasons.addItemDecoration(new HorizontalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_10)));
        DefaultItemAnimator animator = new DefaultItemAnimator();
        rv_seasons.setItemAnimator(animator);


        rv_seasons.setOnItemListener(new TvRecyclerView.OnItemListener() {
            @Override
            public void onItemPreSelected(TvRecyclerView parent, View itemView, int position) {
                Log.d("tag", "test 1");
            }

            @Override
            public void onItemSelected(TvRecyclerView parent, View itemView, int position) {
                Log.d("tag", "test 2");
                setPosition(position);
            }

            @Override
            public void onItemClick(TvRecyclerView parent, View itemView, int position) {
                Log.d("tag", "test 3");
            }
        });
        rv_seasons.setAdapter(adapter);
        setPosition(0);
//        rv_seasons.post(new Runnable() {
//            @Override
//            public void run() {
//                setPosition(0);
//            }
//        });
    }


    private void setPosition(final int position) {
        if (currentPosition != position) {
            currentPosition = position;

            rv_seasons.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                    setTextForList(rv_seasons, position);
                    FrameLayout contentView = getActivity().findViewById(R.id.content_container);
                    if (contentView != null) {
                        EpisodesFilmNew episodesFilm = EpisodesFilmNew.newInstance(datum,
                                position < datum.getSeasons().size() && datum.getType() != 1 ? datum.getSeasons().get(position).getId() : -1);
                        getActivity().getFragmentManager().beginTransaction().replace(contentView.getId(), episodesFilm).commit();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }


    private void setTextForList(View view, int index) {
        TvRecyclerView rv_seasons = (TvRecyclerView) view;
        for (int i = 0; i < rv_seasons.getItemCount(); i++) {
            if (i == index) {
                changeColorForTextView(i);
            } else {
                changeColorForTextView1(i);
            }
        }
    }

    public void changeColorForTextView1(int position) {
        View itemView = rv_seasons.getLayoutManager().findViewByPosition(position);
        if (itemView != null) {
            TextView icon = itemView.findViewById(R.id.tv_season);
            icon.setTextColor(getResources().getColor(R.color.color_text_change));
            icon.setBackgroundResource(R.drawable.season_background);
        }
    }

    public void changeColorForTextView(int position) {
        View itemView = rv_seasons.getLayoutManager().findViewByPosition(position);
        if (itemView != null) {
            TextView icon = itemView.findViewById(R.id.tv_season);
            icon.setTextColor(getResources().getColor(R.color.white));
            icon.setBackgroundResource(R.drawable.season_background_selected);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

}
