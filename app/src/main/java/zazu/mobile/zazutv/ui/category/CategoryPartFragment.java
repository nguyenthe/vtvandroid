package zazu.mobile.zazutv.ui.category;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.com.tvrecyclerview.TvRecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.EndlessScrollListener;
import zazu.mobile.zazutv.adapter.category.CategoryMaulCarouselAdapter;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.model.live.Live;
import zazu.mobile.zazutv.model.menuleft.Item;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.ui.customlib.PlayerActivityNew;
import zazu.mobile.zazutv.ui.detail.MovieDetailsNewActivity;
import zazu.mobile.zazutv.ui.item.VerticalSpaceItemDecoration;

/**
 * Created by Admin on 11/28/2017.
 */

public class CategoryPartFragment extends Fragment {

  private SOService soService;
  private static String KEY = "id";
  private static String TAG = "CategoryPartFragment";
  @BindView(R.id.recyclerview)
  TvRecyclerView tvRecyclerView;
  private CategoryMaulCarouselAdapter adapter;
  private Handler mHandler = new Handler();
  private int currentPage;
  private boolean isNoMorePage;
  private Item item;

  public CategoryPartFragment() {
  }

  public static CategoryPartFragment newInstance() {
    CategoryPartFragment myFragment = new CategoryPartFragment();
    return myFragment;
  }

  public static CategoryPartFragment newInstance(Item someInt) {
    CategoryPartFragment myFragment = new CategoryPartFragment();
    Bundle args = new Bundle();
    args.putSerializable(KEY, someInt);
    myFragment.setArguments(args);
    return myFragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.category_fragment, container, false);
    ButterKnife.bind(this, view);
    currentPage = 1;
    soService = ApiUtils.getSOService();
    showData();
    return view;
  }

  public void showData() {
    GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
    manager.setOrientation(LinearLayoutManager.VERTICAL);
    manager.supportsPredictiveItemAnimations();
    tvRecyclerView.setLayoutManager(manager);
    DefaultItemAnimator animator = new DefaultItemAnimator();
    tvRecyclerView.setItemAnimator(animator);
    tvRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_30)));
    adapter = new CategoryMaulCarouselAdapter(getActivity());
    tvRecyclerView.setAdapter(adapter);
    tvRecyclerView.addOnScrollListener(new EndlessScrollListener(manager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount) {
        loadMore(++currentPage);
      }
    });

    if (getArguments() != null && getArguments().containsKey(KEY)) {
      item = (Item) getArguments().getSerializable(KEY);
      soService.getMoviesByGenreId(item.getReference_slug(), currentPage, 9).enqueue(new Callback<zazu.mobile.zazutv.model.search.filter.Example>() {
        @Override
        public void onResponse(Call<zazu.mobile.zazutv.model.search.filter.Example> call, Response<zazu.mobile.zazutv.model.search.filter.Example> response) {
//                Log.e(TAG, "onResponse " + response.code() + "--- data : " + response.body().LoadEpisodesTask().getItems().size());
          if (response.body().getData() != null) {
            final List<Datum> list = response.body().getData().getTitles();
            adapter = new CategoryMaulCarouselAdapter(getActivity(), list);
            DefaultItemAnimator animator = new DefaultItemAnimator();
            tvRecyclerView.setItemAnimator(animator);
            tvRecyclerView.setAdapter(adapter);
            adapter.setOnItemStateListener(new CategoryMaulCarouselAdapter.OnItemStateListener() {
              @Override
              public void onItemClick(View view, int position) {
                Datum datum = list.get(position);
                Intent intent = new Intent(getActivity(), MovieDetailsNewActivity.class);
                intent.putExtra(MovieDetailsNewActivity.DATUM, datum);
                getActivity().startActivity(intent);
              }
            });

            if(response.body().getData().getPagination().getTotalPages() == currentPage)
              isNoMorePage = true;
          }
        }

        @Override
        public void onFailure(Call<zazu.mobile.zazutv.model.search.filter.Example> call, Throwable t) {

        }
      });
    } else {
      isNoMorePage = true;
      soService.getChannels().enqueue(new Callback<zazu.mobile.zazutv.model.live.Example>() {
        @Override
        public void onResponse(Call<zazu.mobile.zazutv.model.live.Example> call, Response<zazu.mobile.zazutv.model.live.Example> response) {
          if (response.body().getData() != null) {
            final List<Live> list = response.body().getData();
            adapter.setDataChannel(list);
            adapter.setOnItemStateListener(new CategoryMaulCarouselAdapter.OnItemStateListener() {
                  @Override
                  public void onItemClick(View view, int position) {
                  Live live = list.get(position);
                      Intent intent = new Intent(getActivity(), PlayerActivityNew.class);
                      intent.putExtra(Config.LIVE, live.getUrl());
                      startActivity(intent);
                  }
                });

          }
        }

        @Override
        public void onFailure(Call<zazu.mobile.zazutv.model.live.Example> call, Throwable t) {

        }
      });
    }

  }



  public void loadMore(final int start) {
    Log.d("tag", "load more item " + start);
    if (item != null) {
      soService.getMoviesByGenreId(item.getReference_slug(), start, 9).enqueue(new Callback<zazu.mobile.zazutv.model.search.filter.Example>() {
        @Override
        public void onResponse(Call<zazu.mobile.zazutv.model.search.filter.Example> call, Response<zazu.mobile.zazutv.model.search.filter.Example> response) {
//                Log.e(TAG, "onResponse " + response.code() + "--- data : " + response.body().LoadEpisodesTask().getItems().size());
          if (response.body().getData() != null) {
            final List<Datum> list = response.body().getData().getTitles();
            int itemInsert = adapter.getArrayList().size();
            adapter.getArrayList().addAll(list);
            adapter.notifyItemRangeInserted(itemInsert, list.size());

            if(response.body().getData().getPagination().getTotalPages() == start)
              isNoMorePage = true;
          }
        }

        @Override
        public void onFailure(Call<zazu.mobile.zazutv.model.search.filter.Example> call, Throwable t) {

        }
      });
    }
  }



}
