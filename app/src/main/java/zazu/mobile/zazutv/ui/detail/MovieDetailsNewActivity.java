package zazu.mobile.zazutv.ui.detail;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.demo.Icon;
import zazu.mobile.zazutv.model.episodes.Episode;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.sqlitehelper.DatabaseHandler;
import zazu.mobile.zazutv.ui.dialog.episodes.EpisodesFilmNew;

/**
 * Created by Admin on 11/13/2017.
 */

public class MovieDetailsNewActivity extends Activity {

    private static String TAG = "MovieDetailsNewActivity";
    public static final String DATUM = "Datum";
    public static String ID_FILM = "idFilm";
    public static String KEY_SESSION = "keySession";

    private Datum datum = null;
    private List<Icon> mData = null;
    private SOService soService;
    private DatabaseHandler databaseHandler;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.img_Thumb)
    ImageView imgThumb;
    @BindView(R.id.btn_Watch)
    Button btn_Watch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        datum = getIntent().getParcelableExtra(DATUM);
//        Log.e(TAG, "data received : " + " id " + datum.getId() + "--" + datum.getTitle());
        soService = ApiUtils.getSOService();
        databaseHandler = new DatabaseHandler(this);
        init();
    }

    private void init() {
        renderUI(datum);
    }

    EpisodesFilmNew episodesFilm;

    public EpisodesFilmNew getEpisodesFilm() {
        if (episodesFilm == null)
            episodesFilm = (EpisodesFilmNew) getFragmentManager().findFragmentById(R.id.content_container);
        return episodesFilm;
    }

    private void showLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideLoading() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void renderUI(final Datum datum) {
        if (datum != null) {
            hideLoading();
            MovieDetailsMenuFragment menuFragment = MovieDetailsMenuFragment.newInstance(datum);

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction
                    .replace(R.id.header_container, menuFragment, "CustomHeadersFragment");
//                    .replace(R.id.content_container, episodesFilm, "CustomHeadersFragment");
            transaction.commit();

            btn_Watch.setSelected(true);
//            btn_Watch.requestLayout();
            btn_Watch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getEpisodesFilm() != null) {
                        getEpisodesFilm().openMovie();
                    }
                }
            });
            btn_Watch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    btn_Watch.setSelected(hasFocus);
                }
            });
            updateMovie(datum);
        }
    }

    public void updateMovie(Object currentItem) {
        if (currentItem != null) {
//            showLoading();
            String url = "";
            try {
                if (currentItem instanceof Episode) {
                    url = ((Episode) currentItem).getThumbnails().getLandscape().getUrls().get19201080();
                } else if (currentItem instanceof Datum)
                    url = ((Datum) currentItem).getBackgroundImage();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!TextUtils.isEmpty(url))
                Picasso.with(this).load(url)
                        .placeholder(R.drawable.background_home)
                        .error(R.drawable.background_home)
//                        .noFade()
                        .into(imgThumb);

        }
    }
}
