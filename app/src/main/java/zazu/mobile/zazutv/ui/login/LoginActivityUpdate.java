package zazu.mobile.zazutv.ui.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.convert.ConvertUser;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.model.login.Data;
import zazu.mobile.zazutv.model.login.Example;
import zazu.mobile.zazutv.model.login.LoginDB;
import zazu.mobile.zazutv.model.login.User;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.sqlitehelper.DatabaseHandler;
import zazu.mobile.zazutv.ui.customlib.PlayerActivity;

/**
 * Created by Admin on 11/15/2017.
 */

public class LoginActivityUpdate extends Activity implements View.OnClickListener {
    private static String TAG = "Login-Activity";

    @BindView(R.id.ed_Username)
    EditText edUsername;

    @BindView(R.id.ed_Password)
    EditText edPassword;

    @BindView(R.id.btn_Login)
    Button btnLogin;

    private SOService soService;
    private DatabaseHandler db;
    private Datum datum = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        soService = ApiUtils.getSOService();
        db = new DatabaseHandler(this);
        datum = getIntent().getParcelableExtra(Config.DATUM);
        checkAccount();
        clickLogin();
    }


    public void checkAccount() {
        gotDataFromDB gotDataFromDB = new gotDataFromDB();
        gotDataFromDB.execute();
    }

    public class gotDataFromDB extends AsyncTask<Void, LoginDB, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (db.isTableExists("login") == true) {
                if (db.getAllLogin().size() > 0) {
                    LoginDB loginDB = db.getAllLogin().get(0);
                    publishProgress(loginDB);
                }
            } else {
                Log.e(TAG, "no table login");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(LoginDB... values) {
            super.onProgressUpdate(values);
            if (values[0] != null) {
                LoginDB loginDB = values[0];
                edUsername.setText(loginDB.getUsername());
                edPassword.setText(loginDB.getPassword());
            }
        }
    }

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateSignInButtonState();
        }
    };

    public void updateSignInButtonState() {
        btnLogin.setEnabled(edUsername.getText().length() > 0 &&
                edPassword.getText().length() > 0);
    }

    public void clickLogin() {
        btnLogin.setOnClickListener(this);
        btnLogin.setEnabled(false);
        edUsername.addTextChangedListener(tw);
        edPassword.addTextChangedListener(tw);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_Login: {
                loginActivity();
            }
            break;
        }
    }


    public int checkEmpty() {
        if (edUsername.getText().toString().trim().length() > 0 &&
                edPassword.getText().toString().trim().length() > 0) {
            return 1;
        } else if (edUsername.getText().toString().trim().length() == 0 &&
                edPassword.getText().toString().trim().length() == 0) {
            return 2;
        } else {
            return 0;
        }

    }

    public class saveDB extends AsyncTask<LoginDB, Void, Void> {

        @Override
        protected Void doInBackground(LoginDB... loginDBs) {
            LoginDB loginDB = loginDBs[0];
            db.insertLogin(loginDB);
            return null;
        }
    }

    public void loginActivity() {
        if (checkEmpty() == 1) {
            btnLogin.setClickable(true);
            soService.getToken(edUsername.getText().toString(), edPassword.getText().toString()).enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {
                    Log.e(TAG, "code when login : " + response.code() + " status : " + response.body().getStatus());
                    if (response.body().getStatus() == 200) {
                        if (response.body().getData() != null) {
                            Data data = response.body().getData();
                            String token = data.getToken();
                            // convert to Object
                            ConvertUser convertUser = parseToObject(data.getUser(), token);
                            //save to local db
                            saveTokenToDB postToServer = new saveTokenToDB(LoginActivityUpdate.this);
                            postToServer.execute(convertUser);

                            LoginDB loginDB = new LoginDB();
                            loginDB.setUsername(edUsername.getText().toString());
                            loginDB.setPassword(edPassword.getText().toString());

                            saveDB saveDB = new saveDB();
                            saveDB.execute(loginDB);

                            Intent intent = new Intent(LoginActivityUpdate.this, PlayerActivity.class);
                            intent.putExtra(Config.DATUM, datum);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            //ActivityCompat.finishAffinity(LoginActivityUpdate.this);
                        }
                    } else {
                        showAlertDialog();
                    }
                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {
                    Log.d("LoginActivity error: ", "" + t.getMessage() + t.getClass());
                    showAlertDialog();
                }
            });
        } else if (checkEmpty() == 0) {
        } else if (checkEmpty() == 2) {
        }
    }


    public class saveTokenToDB extends AsyncTask<ConvertUser, Void, Void> {

        private Activity activity;

        public saveTokenToDB(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected Void doInBackground(ConvertUser... strings) {
            ConvertUser convertUser = strings[0];
            try {
                if (convertUser != null) {
                    db.insertUser(convertUser);
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    public ConvertUser parseToObject(User user, String token) {
        ConvertUser convertUser = new ConvertUser();
        convertUser.setId(user.getId());
        convertUser.setToken(token);
        convertUser.setUserName(user.getUsername());
        convertUser.setPhoneNumber(user.getProfile().getPhoneNumber().toString());
        convertUser.setAvatar(user.getProfile().getAvatar());
        convertUser.setEmail(user.getEmail());
        return convertUser;
    }

    public void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông Báo");
        builder.setMessage(getResources().getString(R.string.message_error));
        builder.setCancelable(false);
        builder.setPositiveButton("Khôi phục", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                edUsername.setText("");
                edPassword.setText("");
            }
        });
        builder.setNegativeButton("Ðóng", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

}
