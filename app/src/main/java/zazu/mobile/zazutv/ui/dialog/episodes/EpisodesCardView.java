package zazu.mobile.zazutv.ui.dialog.episodes;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.episodes.EpisodesAdapter;
import zazu.mobile.zazutv.model.episodes.Episode;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.ui.base.BindableCardView;
import zazu.mobile.zazutv.ui.customlib.PlayerActivityNew;

/**
 * Created by Admin on 11/11/2017.
 */

public class EpisodesCardView extends BindableCardView<Episode> {

    @BindView(R.id.img_Thumb)
    ImageView mPosterIV;

    public EpisodesCardView(Context context) {
        super(context);
        ButterKnife.bind(this);
    }

    @Override
    public void bind(final Episode data) {
        if (data.getThumbnails().getLandscape() != null)
            Picasso.with(getContext()).load(data.getThumbnails().getLandscape().getUrls().get1280720())
                    .placeholder(R.drawable.background_home)
                    .error(R.drawable.background_home)
                    .into(mPosterIV);
    }

    public ImageView getPosterIV() {
        return mPosterIV;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.item_adapter_horizontal;
    }
}
