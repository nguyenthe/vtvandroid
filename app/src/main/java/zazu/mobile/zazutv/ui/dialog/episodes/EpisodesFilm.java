package zazu.mobile.zazutv.ui.dialog.episodes;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.com.tvrecyclerview.TvRecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.episodes.EpisodesAdapter;
import zazu.mobile.zazutv.convert.ConvertUser;
import zazu.mobile.zazutv.model.episodes.Episode;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.ui.customlib.PlayerActivityNew;

/**
 * Created by Admin on 12/14/2017.
 */

public class EpisodesFilm extends Fragment {

    @BindView(R.id.mRecyclerview)
    TvRecyclerView mRecyclerView1;

    private static final String TAG = "EpisodesFilm";
    private Unbinder unbinder;
    private SOService soService;
    private EpisodesAdapter themeAdapter = null;
    private GridLayoutManager manager;


    public static EpisodesFilm newInstance(Datum someInt) {
        EpisodesFilm episodesFragment = new EpisodesFilm();
        Bundle args = new Bundle();
        args.putParcelable(Config.DATUM, someInt);
        episodesFragment.setArguments(args);
        return episodesFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_episodes, container, false);
        unbinder = ButterKnife.bind(this, view);
        soService = ApiUtils.getSOService();
        getData getData = new getData(getActivity(), mRecyclerView1);
        getData.execute(1);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public class getData extends AsyncTask<Integer, Void, List<ConvertUser>> {
        private Activity activity;
        private RecyclerView mRecyclerView1;

        public getData(Activity activity, RecyclerView recyclerView) {
            this.activity = activity;
            this.mRecyclerView1 = recyclerView;
        }

        int numberImage = 0;

        @Override
        protected List<ConvertUser> doInBackground(Integer... integers) {
            numberImage = integers[0];
            return null;
        }

        @Override
        protected void onPostExecute(List<ConvertUser> authorizations) {
            super.onPostExecute(authorizations);
            final Datum datum = getArguments().getParcelable(Config.DATUM);
            soService.getEpisodesBySession(datum.getId(), datum.getSeasonId())
                    .enqueue(new Callback<zazu.mobile.zazutv.model.episodes.Example>() {
                        @Override
                        public void onResponse(Call<zazu.mobile.zazutv.model.episodes.Example> call,
                                               Response<zazu.mobile.zazutv.model.episodes.Example> response) {
                            Log.e(TAG, "code " + response.code() + " size : " + response.body().getData().getEpisodes().size());
                            if (response.body().getData().getEpisodes() != null) {
                                final List<Episode> listSeason = response.body().getData().getEpisodes();
                                themeAdapter = new EpisodesAdapter(getActivity(),listSeason);
                                manager = new GridLayoutManager(activity, numberImage);
                                mRecyclerView1.setLayoutManager(manager);
                                mRecyclerView1.setHasFixedSize(true);
                                mRecyclerView1.setNestedScrollingEnabled(false);
                                mRecyclerView1.setAdapter(themeAdapter);
                                themeAdapter.setOnItemStateListener(new EpisodesAdapter.OnItemStateListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        Intent intent = new Intent(getActivity(), PlayerActivityNew.class);
                                        //datum.setSeasonId(listSeason.get(position).getId());
                                        //Log.e(TAG, " id season : " + datum.getSeasonId());
                                        Episode episode = listSeason.get(position);
                                        intent.putExtra(Config.LIVE, episode.getFiles().get(0).getUrl());
                                        intent.putExtra(Config.TITLE, episode.getName().toString());
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<zazu.mobile.zazutv.model.episodes.Example> call, Throwable t) {
                            Log.e(TAG, "url : " + call.request() + "---" + t.getMessage());
                        }
                    });
        }
    }

}
