package zazu.mobile.zazutv.ui.category;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.ui.category.menu.MenuFragment;

/**
 * Created by Admin on 11/9/2017.
 */

public class CategoryActivity extends Activity {

    public static Intent getInstance(Context context, int type) {
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.putExtra(TYPE, type);
        return intent;
    }

    public static final int TYPE_MOVIE = 0;
    public static final int TYPE_CHANNEL = TYPE_MOVIE + 1;
    public static final int TYPE_TV_SHOW = TYPE_CHANNEL + 1;
    public static final String TYPE = "TYPE";


    //private VerticalGridFragment verticalGridFragment;
    private CategoryPartFragment categoryPartFragment;
    private MenuFragment menuFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);
        int type = getIntent().getIntExtra(TYPE, 0);
        menuFragment = MenuFragment.newInstance(type);
        if(type == TYPE_CHANNEL)
            categoryPartFragment = CategoryPartFragment.newInstance();
        //verticalGridFragment = VerticalGridFragment.newInstaznce(1);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction
                .replace(R.id.header_container, menuFragment, "CustomHeadersFragment");
        if (categoryPartFragment != null)
            transaction
                    .replace(R.id.category_container, categoryPartFragment, "CustomVerticalFragment");
        transaction.commit();

    }

}
