package zazu.mobile.zazutv.ui.dialog.episodes;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.episodes.TitleEpisodesAdapter;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;

/**
 * Created by Admin on 12/15/2017.
 */

public class TitleSessionFragment extends Fragment {

    private SOService soService;
    private static final String TAG = "EpisodesFilm";
    private Unbinder unbinder;
    private TitleEpisodesAdapter titleEpisodesAdapter;

    @BindView(R.id.nameFilm)
    TextView tvNameFilm;
    @BindView(R.id.tv_Year)
    TextView tvYear;
    @BindView(R.id.tv_Limit)
    TextView tvLimit;
    @BindView(R.id.tv_NumberSession)
    TextView tvNumberSession;
    private int index = -1;

    @BindView(R.id.mRecyclerview)
    com.owen.tvrecyclerview.widget.TvRecyclerView mRecyclerview;

    public TitleSessionFragment() {
    }

    public static TitleSessionFragment newInstance(Datum someInt) {
        TitleSessionFragment episodesFragment = new TitleSessionFragment();
        Bundle args = new Bundle();
        args.putParcelable(Config.DATUM, someInt);
        episodesFragment.setArguments(args);
        return episodesFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.title_session_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initSession();
        return view;
    }

    public void initView() {
        Datum datum = getArguments().getParcelable(Config.DATUM);
        tvLimit.setText(datum.getMaturityRating().getMinAge() + "+");
        tvNameFilm.setText(datum.getTitle());
        tvNumberSession.setText(datum.getSeasons().size() + "");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void initSession() {
        Datum datum = getArguments().getParcelable(Config.DATUM);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.supportsPredictiveItemAnimations();
        mRecyclerview.setLayoutManager(manager);

        mRecyclerview.setLayoutManager(manager);
        DefaultItemAnimator animator = new DefaultItemAnimator();
        mRecyclerview.setItemAnimator(animator);
        Log.e(TAG, " size seasion " + datum.getSeasons());
        titleEpisodesAdapter = new TitleEpisodesAdapter(getActivity(), datum.getSeasons());
        mRecyclerview.setAdapter(titleEpisodesAdapter);
        mRecyclerview.setOnItemListener(new com.owen.tvrecyclerview.widget.TvRecyclerView.OnItemListener() {
            @Override
            public void onItemPreSelected(com.owen.tvrecyclerview.widget.TvRecyclerView parent, View itemView, int position) {

            }

            @Override
            public void onItemSelected(com.owen.tvrecyclerview.widget.TvRecyclerView parent, View itemView, int position) {
                Log.e("Menu fragment", "" + position);
                index = position;
                for (int i = 0; i < mRecyclerview.getItemCount(); i++) {
                    if (i == index) {
                        changeColorForTextView(i);
                    }
                }
                /*FrameLayout contentView = (FrameLayout) getActivity().findViewById(R.id.category_container);
                if (contentView != null) {
                    CategoryPartFragment verticalGridFragment = CategoryPartFragment.newInstance(position+1);
                    getFragmentManager().beginTransaction().replace(contentView.getId(), verticalGridFragment).commit();
                }*/

            }

            @Override
            public void onItemClick(com.owen.tvrecyclerview.widget.TvRecyclerView parent, View itemView, int position) {

            }
        });
    }

    public void changeColorForTextView(int position) {
        View itemView = mRecyclerview.getLayoutManager().findViewByPosition(position);
        TextView icon = itemView.findViewById(R.id.tv_item_tip);
        icon.setBackgroundColor(Color.parseColor("#cfcece"));
        icon.setBackgroundResource(R.drawable.tv_shape_corner);
    }

}
