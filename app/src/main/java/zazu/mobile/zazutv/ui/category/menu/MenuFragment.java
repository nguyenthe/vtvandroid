package zazu.mobile.zazutv.ui.category.menu;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.devs.vectorchildfinder.VectorChildFinder;
import com.devs.vectorchildfinder.VectorDrawableCompat;
import com.owen.tvrecyclerview.TwoWayLayoutManager;
import com.owen.tvrecyclerview.widget.ListLayoutManager;
import com.owen.tvrecyclerview.widget.TvRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.MaulCarouselAdapter;
import zazu.mobile.zazutv.model.menuleft.Example;
import zazu.mobile.zazutv.model.menuleft.Item;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.ui.category.CategoryActivity;
import zazu.mobile.zazutv.ui.category.CategoryPartFragment;
import zazu.mobile.zazutv.ui.item.VerticalSpaceItemDecoration;

/**
 * Created by Admin on 11/16/2017.
 */

public class MenuFragment extends Fragment {

    @BindView(R.id.recyclerview)
    TvRecyclerView mRecyclerview;
    @BindView(R.id.tv_genre_header)
    TextView mTextTitle;
    @BindView(R.id.iv_genre_icon)
    ImageView mImageGenre;
    private SOService soService;
    private int currentIndex = -1;
//    private MaulCarouselAdapter.OnItemStateListener onItemState = new MaulCarouselAdapter.OnItemStateListener() {
//        @Override
//        public void onItemClick(View view, int position) {
//            index = position;
//            setTextForList(list, index);
//        }
//    };
    private List<Item> list;

    public static MenuFragment newInstance(int type) {
        MenuFragment menuFragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putInt(CategoryActivity.TYPE, type);
        menuFragment.setArguments(args);
        return menuFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_menu, container, false);
        ButterKnife.bind(this, view);
        soService = ApiUtils.getSOService();
        init();
        return view;
    }

    public void init() {
        int type = getArguments().getInt(CategoryActivity.TYPE);
        int resourceId = 0;
        switch (type) {
            case CategoryActivity.TYPE_CHANNEL:
                mTextTitle.setText(R.string.live);
                resourceId = (R.drawable.live_icon);
                break;
            case CategoryActivity.TYPE_MOVIE:
                mTextTitle.setText(getString(R.string.movie).toUpperCase());
                resourceId = (R.drawable.movie_icon);
                soService.getMenuMovie().enqueue(callback);
                break;
            case CategoryActivity.TYPE_TV_SHOW:
                mTextTitle.setText(R.string.tv_show);
                resourceId = (R.drawable.tv_show_icon);
                soService.getMenuTvShow().enqueue(callback);
        }
        if (resourceId > 0) {
            mImageGenre.setImageResource(resourceId);
            VectorChildFinder vector = new VectorChildFinder(getActivity(), resourceId, mImageGenre);
            VectorDrawableCompat.VFullPath path = vector.findPathByName("path");
            if (path != null) {
                path.setFillColor(Color.WHITE);
            }
        }
    }

    private Callback<Example> callback = new Callback<Example>() {
        @Override
        public void onResponse(Call<Example> call, Response<Example> response) {
            displayData(response);
        }

        @Override
        public void onFailure(Call<Example> call, Throwable t) {

        }
    };

    private void displayData(Response<Example> response) {
        if (response.code() == 200) {
            if (response.body().getData().getItems() != null && !response.body().getData().getItems().isEmpty()) {
                list = response.body().getData().getItems().get(0).getChildren();
                ListLayoutManager manager = new ListLayoutManager(getActivity(), TwoWayLayoutManager.Orientation.VERTICAL);
//                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
//                manager.setOrientation(LinearLayoutManager.VERTICAL);
//                manager.supportsPredictiveItemAnimations();

                mRecyclerview.setLayoutManager(manager);
                int itemSpace = getResources().
                        getDimensionPixelSize(R.dimen.recyclerView_item_space1);
                mRecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(itemSpace));
                DefaultItemAnimator animator = new DefaultItemAnimator();
                mRecyclerview.setItemAnimator(animator);
                if (list != null && !list.isEmpty()) {
                    final MaulCarouselAdapter mAdapter = new MaulCarouselAdapter(getActivity(), list);
                    mAdapter.setOnItemStateListener(new MaulCarouselAdapter.OnItemStateListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            setPosition(position);
                        }
                    });
                    mRecyclerview.setOnItemListener(new TvRecyclerView.OnItemListener() {
                        @Override
                        public void onItemPreSelected(TvRecyclerView parent, View itemView, int position) {

                        }

                        @Override
                        public void onItemSelected(TvRecyclerView parent, View itemView, int position) {
                            setPosition(position);
                        }

                        @Override
                        public void onItemClick(TvRecyclerView parent, View itemView, int position) {
//                            onItemSelected(parent, itemView, position);
                        }
                    });
//                    mAdapter.setOnItemStateListener(onItemState);
                    mRecyclerview.setAdapter(mAdapter);
                    setPosition(0);
                }
            }
        }
    }

    private void setPosition(final int position){
        if(currentIndex != position){
            currentIndex = position;
            mRecyclerview.post(new Runnable() {
                @Override
                public void run() {
                    setTextForList(list, position);
                }
            });
        }
    }

    private void setTextForList(List<Item> list, int index) {
        for (int i = 0; i < mRecyclerview.getItemCount(); i++) {
            if (i == index) {
                changeColorForTextView(i);
            } else {
                changeColorForTextView1(i);
            }
        }
        FrameLayout contentView = getActivity().findViewById(R.id.category_container);
        if (contentView != null) {
            CategoryPartFragment verticalGridFragment = CategoryPartFragment.newInstance(list.get(index));
            getFragmentManager().beginTransaction().replace(contentView.getId(), verticalGridFragment).commit();
        }
    }

    public void changeColorForTextView1(int position) {
        View itemView = mRecyclerview.getLayoutManager().findViewByPosition(position);
        TextView icon = itemView.findViewById(R.id.tv_item_tip);
        icon.setTextColor(getResources().getColor(R.color.color_text_main_menu));
//        icon.setBackgroundResource(R.drawable.tv_shape_corner);
    }

    public void changeColorForTextView(int position) {
        View itemView = mRecyclerview.getLayoutManager().findViewByPosition(position);
        if (itemView != null) {
            TextView icon = itemView.findViewById(R.id.tv_item_tip);
            icon.setTextColor(getResources().getColor(R.color.white));
        }
    }

}
