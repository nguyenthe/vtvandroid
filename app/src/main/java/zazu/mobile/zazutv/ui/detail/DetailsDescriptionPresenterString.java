package zazu.mobile.zazutv.ui.detail;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;

import zazu.mobile.zazutv.model.homepage.theme.Datum;

/**
 * Created by Admin on 11/14/2017.
 */

public class DetailsDescriptionPresenterString extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Datum movie = (Datum) item;
        if (movie != null) {
            viewHolder.getTitle().setText(movie.getTitle());
            viewHolder.getBody().setText(movie.getDescription());
        }
    }
}

