package zazu.mobile.zazutv.ui.item;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.demo.Icon;
import zazu.mobile.zazutv.ui.base.BindableCardView;

/**
 * Created by Admin on 11/11/2017.
 */

public class IconHeaderCardView extends BindableCardView<Icon> {

    @BindView(R.id.img_Icon)
    ImageView mPosterIV;

    @BindView(R.id.tv_Title)
    TextView tvLable;

    public IconHeaderCardView(Context context) {
        super(context);
        ButterKnife.bind(this);
    }

    @Override
    public void bind(Icon data) {
        mPosterIV.setImageResource(data.getIdSource());
        tvLable.setText(data.getTitle());
    }

    public ImageView getPosterIV() {
        return mPosterIV;
    }

    public TextView getTvLable() {
        return tvLable;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.icon_header_griditem;
    }
}
