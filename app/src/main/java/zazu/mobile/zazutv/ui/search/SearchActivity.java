package zazu.mobile.zazutv.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.ui.general.LeanbackActivity;

/**
 * Created by Admin on 11/10/2017.
 */

public class SearchActivity extends LeanbackActivity {
    private SearchFragment searchFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        searchFragment = (SearchFragment) getFragmentManager().findFragmentById(R.id.search_fragment);
    }

    @Override
    public boolean onSearchRequested() {
        if (searchFragment.hasResults()) {
            startActivity(new Intent(this, SearchActivity.class));
        } else {
            searchFragment.startRecognition();
        }
        return true;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // If there are no results found, press the left key to reselect the microphone
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT && !searchFragment.hasResults()) {
            searchFragment.focusOnSearch();
        }
        return super.onKeyDown(keyCode, event);
    }
}
