package zazu.mobile.zazutv.ui.item;

import android.content.Context;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.ui.base.BindableCardView;

/**
 * Created by Admin on 11/11/2017.
 */

public class StringView extends BindableCardView<Datum> {

    @BindView(R.id.tv_item_tip)
    Button tvTitle;

    public StringView(Context context) {
        super(context);
        ButterKnife.bind(this);
    }

    @Override
    public void bind(Datum data) {
        if(data != null){
            tvTitle.setText(data.getTitle());
        }
    }

    public Button getTvTitle() {
        return tvTitle;
    }

    public void setTvTitle(Button tvTitle) {
        this.tvTitle = tvTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.header_category;
    }
}
