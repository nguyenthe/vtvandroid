package zazu.mobile.zazutv.ui.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.com.tvrecyclerview.TvRecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.EndlessScrollListener;
import zazu.mobile.zazutv.adapter.search.MaulCarouselAdapter;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.ui.detail.MovieDetailsNewActivity;
import zazu.mobile.zazutv.ui.item.VerticalSpaceItemDecoration;
import zazu.mobile.zazutv.ui.keyboard.CustomEasyTVKeyboard;

/**
 * Created by Admin on 11/18/2017.
 */

public class SearchZazuActivity extends Activity implements CustomEasyTVKeyboard.OnMyTextChangedListener, View.OnClickListener {
    private static String TAG = SearchZazuActivity.class.getSimpleName();
    private CustomEasyTVKeyboard customEasyTVKeyboard;
    private Button btnBack;
    private TextView tvResult;
    private TvRecyclerView tvRecyclerView;
    private SOService soService;
    private Handler handler = new Handler();
    private MaulCarouselAdapter adapter;
    private List<Datum> mData = new ArrayList<>();
    private GridLayoutManager manager;
    private int currentPage = 1;
    private String mContent;
    private boolean isNoMorePage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        soService = ApiUtils.getSOService();
        customEasyTVKeyboard = findViewById(R.id.custom_Keyboard);
        tvRecyclerView = findViewById(R.id.mRecyclerview);
        btnBack = findViewById(R.id.btn_Back);
        tvResult = findViewById(R.id.no_Result);
        btnBack.setOnClickListener(this);
        customEasyTVKeyboard.setmOnMyTextChangedListener(this);

        manager = new GridLayoutManager(this, 4);
//        GridLayoutManager manager = new GridLayoutManager(SearchZazuActivity.this, 4);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.supportsPredictiveItemAnimations();
        tvRecyclerView.setLayoutManager(manager);
        tvRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_30)));
//        tvRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_10)));
        DefaultItemAnimator animator = new DefaultItemAnimator();
        tvRecyclerView.setItemAnimator(animator);
        tvRecyclerView.addOnScrollListener(new EndlessScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMore(++currentPage, mContent);
            }
        });

        renderSearch("");
    }

    public void init(final List<Datum> list, final String content) {
        tvResult.setVisibility(View.GONE);
        adapter = new MaulCarouselAdapter(SearchZazuActivity.this, list);
        tvRecyclerView.setAdapter(adapter);
        tvRecyclerView.setVisibility(View.VISIBLE);
        adapter.setOnItemStateListener(new MaulCarouselAdapter.OnItemStateListener() {
            @Override
            public void onItemClick(View view, int position) {
                Datum datum = list.get(position);
                Intent intent = new Intent(SearchZazuActivity.this, MovieDetailsNewActivity.class);
                intent.putExtra(MovieDetailsNewActivity.DATUM, datum);
                startActivity(intent);
            }
        });
    }

    public void renderSearch(final String content) {
        if (TextUtils.isEmpty(content)) {
            tvResult.setVisibility(View.GONE);
            tvRecyclerView.setVisibility(View.GONE);
            isNoMorePage = true;
            mContent = "";
            return;
        }
        if (!content.equals(mContent)) {
            currentPage = 1;
            mContent = content;
            isNoMorePage = false;
        }
        soService.getDataSearch(content, currentPage, 20).enqueue(new Callback<zazu.mobile.zazutv.model.search.search.Example>() {
            @Override
            public void onResponse(Call<zazu.mobile.zazutv.model.search.search.Example> call, Response<zazu.mobile.zazutv.model.search.search.Example> response) {
                mData = response.body().getData().getTitles();
                Log.e(TAG, "size zazu_List : " + mData.size());

                if (mData.size() > 0) {
                    init(mData, content);
                } else if (mData.size() == 0) {
                    tvResult.setVisibility(View.VISIBLE);
                    tvRecyclerView.setVisibility(View.GONE);
                }
                if(currentPage == response.body().getData().getPagination().getTotalPages())
                    isNoMorePage = true;
            }

            @Override
            public void onFailure(Call<zazu.mobile.zazutv.model.search.search.Example> call, Throwable t) {
                Log.e("Search-Activity", " error " + call.request() + " -- " + t.getMessage());
            }
        });
    }

    @Override
    public void afterTextChaned(String text) {
    }

    @Override
    public void onTextChanged(String text) {
        Log.d(TAG, "text change : " + text);
        if (text.length() > 0) {
            renderSearch(text);
        } else {
            renderSearch("");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_Back: {
                onBackPressed();
            }
            break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (customEasyTVKeyboard.hasFocus()) {
//                String text = customEasyTVKeyboard.getSearchText();
//                Log.i(TAG, "-------- onKeyDown ----------text=" + text);
//                if (!TextUtils.isEmpty(text)) {
//                    customEasyTVKeyboard.deleteSearchText();
//                    return true;
//                }
//            }
//        }
        return super.onKeyDown(keyCode, event);
    }

    public void loadMore(final int start, final String content) {
        Log.d("tag", "load more item " + start);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                soService.getDataSearch(content, start, 20)
                        .enqueue(new Callback<zazu.mobile.zazutv.model.search.search.Example>() {
                            @Override
                            public void onResponse(Call<zazu.mobile.zazutv.model.search.search.Example> call,
                                                   Response<zazu.mobile.zazutv.model.search.search.Example> response) {
                                List<Datum> list = response.body().getData().getTitles();
                                if (!list.isEmpty()) {
                                    isNoMorePage = true;
                                    int itemInsert = mData.size();
                                    mData.addAll(list);
                                    adapter.notifyItemRangeInserted(itemInsert, list.size());
                                    tvResult.setVisibility(View.GONE);
                                }

                                if(currentPage == response.body().getData().getPagination().getTotalPages())
                                    isNoMorePage = true;
                            }

                            @Override
                            public void onFailure(Call<zazu.mobile.zazutv.model.search.search.Example> call, Throwable t) {

                            }
                        });
            }
        }, 200);
    }

}
