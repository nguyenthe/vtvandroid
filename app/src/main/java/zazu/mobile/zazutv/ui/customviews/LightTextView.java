package zazu.mobile.zazutv.ui.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by ZINNO on 4/13/2018.
 */

public class LightTextView extends AppCompatTextView {


  public LightTextView(Context context) {
    super(context);
    init(context);
  }

  public LightTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public LightTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  private void init(Context context){
    if(!isInEditMode()){
      Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
      this.setTypeface(face);
    }
  }

}
