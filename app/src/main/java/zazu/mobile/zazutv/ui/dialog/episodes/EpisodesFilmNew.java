package zazu.mobile.zazutv.ui.dialog.episodes;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.owen.tvrecyclerview.TwoWayLayoutManager;
import com.owen.tvrecyclerview.widget.ListLayoutManager;
import com.owen.tvrecyclerview.widget.TvRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.category.CategoryMaulCarouselAdapter;
import zazu.mobile.zazutv.model.episodes.Episode;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.ui.customlib.PlayerActivityNew;
import zazu.mobile.zazutv.ui.detail.MovieDetailsNewActivity;
import zazu.mobile.zazutv.ui.detail.SeasonControl;
import zazu.mobile.zazutv.ui.item.HorizontalSpaceItemDecoration;

/**
 * Created by Admin on 12/14/2017.
 */

public class EpisodesFilmNew extends Fragment implements SeasonControl {
    private static final String TAG = "EpisodesFilmNew";
    public static final String SEASON_ID = "SEASON_ID";

    Callback callback;
    private SOService soService;
    int seasonId = 0;
    boolean isSeason;
    Datum datum;
    int currentEpisode = 0;
    List<Episode> episodes = new ArrayList<>();
    List<Datum> datums = new ArrayList<>();
    Object currentItem;

    @BindView(R.id.rv_seasons)
    TvRecyclerView rv_seasons;
    private CategoryMaulCarouselAdapter adapter;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    public static EpisodesFilmNew newInstance(Datum someInt, int seasonId) {
        EpisodesFilmNew episodesFragment = new EpisodesFilmNew();
        Bundle args = new Bundle();
        args.putParcelable(Config.DATUM, someInt);
        args.putInt(SEASON_ID, seasonId);
        episodesFragment.setArguments(args);
        return episodesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_content_fragment, container, false);
        ButterKnife.bind(this, view);
        soService = ApiUtils.getSOService();

        init();
        return view;
    }

    private void init() {
        progressBar.setVisibility(View.VISIBLE);
        ListLayoutManager manager = new ListLayoutManager(getActivity(), TwoWayLayoutManager.Orientation.HORIZONTAL);
        rv_seasons.setLayoutManager(manager);

        adapter = new CategoryMaulCarouselAdapter(mContext, new ArrayList<Datum>());
        rv_seasons.setAdapter(adapter);
        rv_seasons.addItemDecoration(new HorizontalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycleview_item_spacing_10)));
        DefaultItemAnimator animator = new DefaultItemAnimator();
        rv_seasons.setItemAnimator(animator);

        adapter.setOnItemStateListener(new CategoryMaulCarouselAdapter.OnItemStateListener() {
            @Override
            public void onItemClick(View view, int position) {
                setPosition(position);
            }
        });
        rv_seasons.setOnItemListener(new TvRecyclerView.OnItemListener() {
            @Override
            public void onItemPreSelected(TvRecyclerView parent, View itemView, int position) {

            }

            @Override
            public void onItemSelected(TvRecyclerView parent, View itemView, int position) {
                setCurrentItem(position);
            }

            @Override
            public void onItemClick(TvRecyclerView parent, View itemView, int position) {

            }
        });
        load();
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void setCurrentItem(int position) {
        if (isSeason)
            currentItem = episodes.get(position);
        else currentItem = datums.get(position);
        ((MovieDetailsNewActivity) mContext).updateMovie(currentItem);
    }

    public void load() {
        setupUIElements();
        progressBar.setVisibility(View.VISIBLE);
        datum = getArguments().getParcelable(Config.DATUM);
        seasonId = getArguments().getInt(SEASON_ID);
        currentEpisode = 0;
        if (seasonId == -1)
            loadSimilarDatum();
        else
            getData(seasonId);
    }

    public void initData(List<Episode> listSeason) {
        adapter.setDataEpisodes(listSeason);
        episodes = listSeason;
        setCurrentItem(0);
//        adapter.setOnItemStateListener(new CategoryMaulCarouselAdapter.OnItemStateListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                setPosition(position);
//            }
//        });
//        rv_seasons.setOnItemListener(new TvRecyclerView.OnItemListener() {
//            @Override
//            public void onItemPreSelected(TvRecyclerView parent, View itemView, int position) {
//                Log.d("tag", "test 1");
//            }
//
//            @Override
//            public void onItemSelected(TvRecyclerView parent, View itemView, int position) {
//                Log.d("tag", "test 2");
//                setPosition(position);
//            }
//
//            @Override
//            public void onItemClick(TvRecyclerView parent, View itemView, int position) {
//                Log.d("tag", "test 3");
//            }
//        });
//        rv_seasons.post(new Runnable() {
//            @Override
//            public void run() {
////                setPosition(rv_seasons, 0);
//            }
//        });
    }

    public void loadDatum(List<Datum> listDatum) {
        adapter.setDataMovie(listDatum);
        datums = listDatum;

        setCurrentItem(0);
    }

    private void setPosition(int i) {
        try {
            if (isSeason) {
                openEpisode(episodes.get(i));
            } else
                openDatumType(datums.get(i));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData(int seasonId) {
        isSeason = true;
        soService.getEpisodesBySession(datum.getId(), seasonId)
                .enqueue(new Callback<zazu.mobile.zazutv.model.episodes.Example>() {
                    @Override
                    public void onResponse(Call<zazu.mobile.zazutv.model.episodes.Example> call,
                                           Response<zazu.mobile.zazutv.model.episodes.Example> response) {
                        Log.e(TAG, "code " + response.code() + " size : " + response.body().getData().getEpisodes().size());
                        if (response.body().getData().getEpisodes() != null) {
                            initData(response.body().getData().getEpisodes());
                        }
                        if (callback != null) {
                            callback.onResponse(call, response);
                        }
                    }

                    @Override

                    public void onFailure(Call<zazu.mobile.zazutv.model.episodes.Example> call, Throwable t) {
                        if (callback != null) {
                            callback.onFailure(call, t);
                        }
                    }
                });
    }

    private void loadSimilarDatum() {
        isSeason = false;
        soService.getSimilar(datum.getId()).enqueue(new Callback<zazu.mobile.zazutv.model.search.search.Example>() {
            @Override
            public void onResponse(Call<zazu.mobile.zazutv.model.search.search.Example> call, Response<zazu.mobile.zazutv.model.search.search.Example> response) {
                List<Datum> data = new ArrayList<>();
                if (response.body().getData().getTitles() != null) {
                    data = response.body().getData().getTitles();
                }
                if (datum.getType() == 1)
                    data.add(0, datum);
                loadDatum(data);
                if (callback != null) {
                    callback.onResponse(call, response);
                }
            }

            @Override
            public void onFailure(Call<zazu.mobile.zazutv.model.search.search.Example> call, Throwable t) {
                if (callback != null) {
                    callback.onFailure(call, t);
                }
            }
        });
    }

    public void openEpisode(Episode episode) {
//        setSelectedPosition(0, true, new ListRowPresenter.SelectItemViewHolderTask(currentEpisode));
        currentEpisode = episode.getIndex();
        datum.setEpisodeId(episode.getId());
        datum.setSeasonId(seasonId);
        datum.setEpisodeIndex(currentEpisode);
        openDatumType(datum);
    }

    public void openDatumType(Datum datum) {
        Intent intent = new Intent(mContext, PlayerActivityNew.class);
//        intent.putExtra(Config.LIVE, datum.get.getFiles().get(0).getUrl());
        intent.putExtra(Config.DATUM, datum);
        startActivity(intent);
    }

    private void setupUIElements() {
        // over title
//        setAlignment(0);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void openNext() {
        if (currentEpisode < episodes.size()) {
            openEpisode(episodes.get(currentEpisode));
        }
    }

    @Override
    public void loadSimilar() {
        loadSimilarDatum();
    }

    @Override
    public void loadEpisodes(int seasonId) {
        getData(seasonId);
    }

    public void openMovie() {
        if (currentItem != null)
            if (currentItem instanceof Episode) {
                openEpisode((Episode) currentItem);
            } else
                openDatumType((Datum) currentItem);
    }

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
