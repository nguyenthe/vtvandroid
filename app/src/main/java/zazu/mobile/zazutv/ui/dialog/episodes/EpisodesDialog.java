package zazu.mobile.zazutv.ui.dialog.episodes;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.common.Config;

/**
 * Created by Admin on 12/15/2017.
 */

public class EpisodesDialog extends DialogFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
    }

    public static EpisodesDialog newInstance(Datum someInt) {
        EpisodesDialog episodesFragment = new EpisodesDialog();
        Bundle args = new Bundle();
        args.putParcelable(Config.DATUM, someInt);
        episodesFragment.setArguments(args);
        return episodesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.espisodes_fragment, container, false);
        initTitleSession();
        return view;
    }

    public void initTitleSession() {
        Datum datum = getArguments().getParcelable(Config.DATUM);
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        TitleSessionFragment titleSessionFragment = TitleSessionFragment.newInstance(datum);
        EpisodesFilm episodesFilm = EpisodesFilm.newInstance(datum);
        transaction
                .replace(R.id.title_session, titleSessionFragment, "titleSession")
                .replace(R.id.content_episodes, episodesFilm, "");
        transaction.commit();
    }


}
