package zazu.mobile.zazutv.ui.detail;

/**
 * Created by User on 4/18/2018.
 */

public interface SeasonControl {
    void openNext();
    void loadSimilar();
    void loadEpisodes(int seasonId);
}
