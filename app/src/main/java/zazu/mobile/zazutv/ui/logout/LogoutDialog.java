package zazu.mobile.zazutv.ui.logout;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import zazu.mobile.zazutv.MainFragment;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.convert.ConvertUser;
import zazu.mobile.zazutv.sqlitehelper.DatabaseHandler;

/**
 * Created by Admin on 12/4/2017.
 */

public class LogoutDialog extends DialogFragment implements View.OnClickListener {
  private Unbinder unbinder;
  @BindView(R.id.profile_image)
  CircleImageView circleImageView;
  @BindView(R.id.btn_Logout)
  Button btnLogout;
  @BindView(R.id.btn_Close)
  Button btnCancel;
  @BindView(R.id.tvWarning)
  TextView tvWarning;
  private DatabaseHandler databaseHandler;

  public LogoutDialog() {
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.log_out, container, false);
    ButterKnife.bind(this, view);
    databaseHandler = new DatabaseHandler(getActivity());
    getViewFromLayout();
    return view;
  }

  public void getViewFromLayout() {
    btnCancel.setOnClickListener(this);
    btnLogout.setOnClickListener(this);
    updateUI updateUI = new updateUI(getActivity());
    updateUI.execute("");
  }

  public class updateUI extends AsyncTask<String, ConvertUser, String> {

    private Activity activity;

    public updateUI(Activity activity) {
      this.activity = activity;
    }

    @Override
    protected String doInBackground(String... voids) {
      if (databaseHandler.getAllUser().size() > 0) {
        List<ConvertUser> list = databaseHandler.getAllUser();
        publishProgress(list.get(0));
      }
      return null;
    }

    @Override
    protected void onProgressUpdate(ConvertUser... values) {
      super.onProgressUpdate(values);
      if (values != null) {
        ConvertUser convertUser = values[0];
        if (!convertUser.getAvatar().isEmpty()) {
          Picasso.with(getActivity())
                  .load(convertUser.getAvatar())
                  .placeholder(R.drawable.background_home)
                  .error(R.drawable.background_home)
                  .into(circleImageView);
          tvWarning.setText(activity.getResources().getString(R.string.warm_logout));
        } else {
          tvWarning.setText(activity.getResources().getString(R.string.warm_logout));
          circleImageView.setVisibility(View.GONE);
        }
      }
    }
  }

  public class removeAccount extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... voids) {
      databaseHandler.deleteAllUser();
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (getTargetFragment() != null) {
        getTargetFragment().onActivityResult(MainFragment.REQUEST_LOGOUT, Activity.RESULT_OK, new Intent());
      }
      getDialog().dismiss();
    }
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btn_Logout: {
        removeAccount removeAccount = new removeAccount();
        removeAccount.execute();
      }
      break;
      case R.id.btn_Close: {
        getDialog().dismiss();
      }
      break;
    }
  }
}
