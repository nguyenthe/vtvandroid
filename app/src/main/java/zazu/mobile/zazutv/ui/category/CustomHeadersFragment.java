package zazu.mobile.zazutv.ui.category;

import android.os.Bundle;
import android.support.v17.leanback.app.HeadersFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowHeaderPresenter;
import android.util.Log;
import android.widget.FrameLayout;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.menuleft.Example;
import zazu.mobile.zazutv.model.menuleft.Item;
import zazu.mobile.zazutv.presenter.StringPresenter;
import zazu.mobile.zazutv.presenter.VerticalGridFragment;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;

/**
 * Created by Admin on 11/13/2017.
 */

public class CustomHeadersFragment extends HeadersFragment {

    private ArrayObjectAdapter adapter;
    private SOService soService;
    private static final String TAG = "CustomHeader-Fragment";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        soService = ApiUtils.getSOService();
        customSetBackground(R.color.background_gradient_start);
        getCategoryList();
        setupEventListeners();
    }

    private void getCategoryList() {
        adapter = new ArrayObjectAdapter(new ListRowPresenter());
        soService.getMenuLeft(1).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if (response.code() == 200) {
                    List<Item> items = response.body().getData().getItems();
                    for (int i = 0; i < items.size(); ++i) {
                        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(
                                new StringPresenter());
                        listRowAdapter.add(items.get(i).getId());
                        HeaderItem header = new HeaderItem(i, items.get(i).getName());
                        adapter.add(new ListRow(header, listRowAdapter));
                    }
                    setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

            }
        });
    }

    private void setupEventListeners() {
        setOnHeaderViewSelectedListener(new ItemViewSelectedListener());
        setOnHeaderClickedListener(new ItemViewClickedListener());
    }

    private final class ItemViewClickedListener implements OnHeaderClickedListener {

        @Override
        public void onHeaderClicked(RowHeaderPresenter.ViewHolder viewHolder, Row row) {
            if (viewHolder != null) {
                int obj = (int) ((ListRow) row).getAdapter().get(0);
                if (obj != -1) {
                    Log.e(TAG, " info : " + obj);
                    FrameLayout contentView = (FrameLayout) getActivity().findViewById(R.id.category_container);
                    if (contentView != null) {
                        VerticalGridFragment verticalGridFragment = VerticalGridFragment.newInstance(obj);
                        getFragmentManager().beginTransaction().replace(contentView.getId(), verticalGridFragment).commit();
                    }
                }
            }
        }
    }

    private final class ItemViewSelectedListener implements OnHeaderViewSelectedListener {
        @Override
        public void onHeaderSelected(RowHeaderPresenter.ViewHolder viewHolder, Row row) {
            if (viewHolder != null) {
                viewHolder.view.setBackgroundResource(R.drawable.selector);
                Log.d(TAG, "row id : " + row.getHeaderItem().getId());
                int obj = (int) ((ListRow) row).getAdapter().get(0);
                if (obj != -1) {
                    if(obj == 1){
                        viewHolder.view.setBackgroundResource(R.drawable.selector);
                    }
                    Log.e(TAG, " info : " + obj);
                    /*FrameLayout contentView = (FrameLayout) getActivity().findViewById(R.id.category_container);
                    if (contentView != null) {
                        VerticalGridFragment verticalGridFragment = VerticalGridFragment.newInstance(obj);
                        getFragmentManager().beginTransaction().replace(contentView.getId(), verticalGridFragment).commit();
                    }*/
                }
            }
        }
    }

    private void customSetBackground(int colorResource) {
        try {
            Class clazz = HeadersFragment.class;
            Method m = clazz.getDeclaredMethod("setBackgroundColor", Integer.TYPE);
            m.setAccessible(true);
            m.invoke(this, getResources().getColor(colorResource));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /*public class StringPresenter extends Presenter {
        private static final String TAG = "StringPresenter";

        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            TextView textView = new TextView(parent.getContext());
            textView.setFocusable(true);
            textView.setFocusableInTouchMode(true);
            int[][] states = new int[][]{
                    new int[]{android.R.attr.state_pressed}, // pressed
                    new int[]{android.R.attr.state_focused}, // focused
                    new int[]{}
            };
            int[] colors = new int[]{
                    (R.color.pressed), // green
                    (R.color.background_gradient_end), // green
                    (R.color.white)  // white
            };
            ColorStateList list = new ColorStateList(states, colors);
            textView.setTextColor(list);
            textView.setBackground(
                    parent.getContext().getResources().getDrawable(R.color.amber));
            return new ViewHolder(textView);
        }

        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ((TextView) viewHolder.view).setText(item.toString());
        }

        public void onUnbindViewHolder(ViewHolder viewHolder) {
            // no op
        }
    }*/

}