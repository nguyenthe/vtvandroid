package zazu.mobile.zazutv.ui.dialog.episodes;

import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;

import zazu.mobile.zazutv.model.episodes.Episode;

public class EpisodesPresenter extends Presenter {
    public EpisodesPresenter(){}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        EpisodesCardView movieCardView = new EpisodesCardView(parent.getContext());
        movieCardView.setFocusable(true);
        movieCardView.setFocusableInTouchMode(true);
        return new ViewHolder(movieCardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ((EpisodesCardView) viewHolder.view).bind((Episode) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }


}
