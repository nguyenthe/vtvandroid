package zazu.mobile.zazutv.ui.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by ZINNO on 4/13/2018.
 */

public class RegularButton extends AppCompatButton {


  public RegularButton(Context context) {
    super(context);
    init(context);
  }

  public RegularButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public RegularButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  private void init(Context context){
    if(!isInEditMode()){
      Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
      this.setTypeface(face);
    }
  }

}
