package zazu.mobile.zazutv.ui.search;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.BuildConfig;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.ObjectAdapter;
import android.support.v17.leanback.widget.VerticalGridPresenter;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.CardPresenter;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.model.search.search.Example;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;

/**
 * Created by Admin on 11/10/2017.
 */

public class SearchFragment extends android.support.v17.leanback.app.SearchFragment
        implements android.support.v17.leanback.app.SearchFragment.SearchResultProvider,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "SearchFragment";
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final boolean FINISH_ON_RECOGNIZER_CANCELED = true;
    private static final int REQUEST_SPEECH = 0x00000010;

    private final Handler mHandler = new Handler();
    private ArrayObjectAdapter mRowsAdapter;
    private String mQuery;
    private final CursorObjectAdapter mVideoCursorAdapter =
            new CursorObjectAdapter(new CardPresenter());

    private int mSearchLoaderId = 1;
    private boolean mResultsFound = false;
    private SOService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = ApiUtils.getSOService();
        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        setSearchResultProvider(this);
    }

    //check Adapter has result
    public boolean hasResults() {
        return mRowsAdapter.size() > 0 && mResultsFound;
    }

    private boolean hasPermission(final String permission) {
        final Context context = getActivity();
        return PackageManager.PERMISSION_GRANTED == context.getPackageManager().checkPermission(
                permission, context.getPackageName());
    }

    private void loadQuery(String query) {
        if (!TextUtils.isEmpty(query) && !query.equals("nil")) {
            mQuery = query;
            getLoaderManager().initLoader(mSearchLoaderId++, null, this);
        }
    }

    public void focusOnSearch() {
        getView().findViewById(R.id.lb_search_bar).requestFocus();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public void loadRow() {
        new AsyncTask<String, List<Datum>, List<Datum>>() {
            private final String query = mQuery;

            @Override
            protected List<Datum> doInBackground(String... strings) {
                service.getDataSearch(query, 1, 10).enqueue(new Callback<Example>() {
                    @Override
                    public void onResponse(Call<Example> call, Response<Example> response) {
                        if (response.code() == 200) {
                            Log.d(TAG, "size " + response.body().getData().getTitles().size());
                            publishProgress(response.body().getData().getTitles());
                        }
                    }

                    @Override
                    public void onFailure(Call<Example> call, Throwable t) {
                        Log.d(TAG, "FAILED " + t.getMessage() + "---" + call.request());
                    }
                });
                return null;
            }

            @Override
            protected void onProgressUpdate(List<Datum>... values) {
                super.onProgressUpdate(values);
                List<Datum> result = values[0];
                CardPresenter cardPresenter_1 = new CardPresenter();
                ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(cardPresenter_1);
                for (int j = 0; j < result.size(); j++) {
                    Datum movie = result.get(j);
                    cardRowAdapter.add(movie);
                }
                HeaderItem header = new HeaderItem(3, mQuery);
                mRowsAdapter.clear();

                VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
                gridPresenter.setNumberOfColumns(3);
                ListRow row = new ListRow(header, cardRowAdapter);
                mRowsAdapter.add(row);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public ObjectAdapter getResultsAdapter() {
        return mRowsAdapter;
    }

    @Override
    public boolean onQueryTextChange(String newQuery) {
        mQuery = newQuery;
        loadRow();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mQuery = query;
        loadRow();
        return true;
    }
}
