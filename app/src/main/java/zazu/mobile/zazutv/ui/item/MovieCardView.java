package zazu.mobile.zazutv.ui.item;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.ui.base.BindableCardView;

/**
 * Created by Admin on 11/11/2017.
 */

public class MovieCardView extends BindableCardView<Datum> {

    @BindView(R.id.img_Thumb)
    ImageView mPosterIV;

    public MovieCardView(Context context) {
        super(context);
        ButterKnife.bind(this);
    }

    @Override
    public void bind(Datum data) {
        Glide.with(getContext())
                .load(data.getBackgroundImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.icon_default_landscape)
                .into(mPosterIV);
    }

//    public ImageView getPosterIV() {
//        return mPosterIV;
//    }

    @Override
    protected int getLayoutResource() {
        return R.layout.item_adapter_horizontal_main;
    }
}
