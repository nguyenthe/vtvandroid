/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package zazu.mobile.zazutv.ui.category;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.VerticalGridPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.net.URI;
import java.util.List;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.BrowseErrorActivity;
import zazu.mobile.zazutv.DetailsActivity;
import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;

public class CategoryFragment extends BrowseFragment {
    private static final String TAG = "MainFragment";

    private static final int BACKGROUND_UPDATE_DELAY = 300;
    private static final int NUM_ROWS = 6;
    private static final int NUM_COLS = 15;
    private static final int DETAIL_THUMB_WIDTH = 274;
    private static final int DETAIL_THUMB_HEIGHT = 274;
    private final Handler mHandler = new Handler();
    private ArrayObjectAdapter mRowsAdapter;
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private Timer mBackgroundTimer;
    private URI mBackgroundURI;
    private BackgroundManager mBackgroundManager;
    private SOService soService;
    ImageView imageView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        soService = ApiUtils.getSOService();
        setupUIElements();
        prepareBackgroundManager();
        getLog getLog = new getLog(getActivity());
        getLog.execute();
        setupEventListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mBackgroundTimer) {
            Log.d(TAG, "onDestroy: " + mBackgroundTimer.toString());
            mBackgroundTimer.cancel();
        }
    }


    private void prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground = getResources().getDrawable(R.drawable.default_background);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void setupUIElements() {
        // over title
        setHeadersState(HEADERS_DISABLED); //HEADERS_DISABLED
        setHeadersTransitionOnBackEnabled(true); //true
        // set fastLane (or headers) background color
        //setBrandColor(getResources().getColor(R.color.fastlane_background));
        // set search icon color
        //setSearchAffordanceColor(getResources().getColor(R.color.search_opaque));// setBadgeDrawable(getActivity().getResources().getDrawable(
    }

    private void setupEventListeners() {
        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }


    public class getLog extends AsyncTask<Void, zazu.mobile.zazutv.model.menuleft.Item, List<zazu.mobile.zazutv.model.menuleft.Item>> {

        private Activity activity;

        public getLog(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected List<zazu.mobile.zazutv.model.menuleft.Item> doInBackground(Void... voids) {
            soService.getMenuLeft(1).enqueue(new Callback<zazu.mobile.zazutv.model.menuleft.Example>() {
                @Override
                public void onResponse(Call<zazu.mobile.zazutv.model.menuleft.Example> call, Response<zazu.mobile.zazutv.model.menuleft.Example> response) {
                    if (response.code() == 200) {
                        List<zazu.mobile.zazutv.model.menuleft.Item> items = response.body().getData().getItems();
                        for (int i = 0; i < items.size(); i++) {
                            publishProgress(items.get(i));
                        }
                    }
                }

                @Override
                public void onFailure(Call<zazu.mobile.zazutv.model.menuleft.Example> call, Throwable t) {

                }
            });
            return null;
        }

        @Override
        protected void onProgressUpdate(zazu.mobile.zazutv.model.menuleft.Item... values) {
            super.onProgressUpdate(values);
            if (values[0] != null) {
                zazu.mobile.zazutv.model.menuleft.Item item = values[0];
                mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
                HeaderItem header = new HeaderItem(item.getName());
                ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(new VerticalGridPresenter());

            }
        }

    }


    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Datum) {
                Datum datum = (Datum) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(DetailsActivity.VIDEO, datum);
                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        ((ImageCardView) itemViewHolder.view).getMainImageView(),
                        DetailsActivity.SHARED_ELEMENT_NAME).toBundle();
                getActivity().startActivity(intent, bundle);
            } else if (item instanceof String) {
                if (((String) item).indexOf(getString(R.string.error_fragment)) >= 0) {
                    Intent intent = new Intent(getActivity(), BrowseErrorActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), ((String) item), Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {

        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Datum) {

            }

        }
    }


}
