package zazu.mobile.zazutv.adapter.menu;

/**
 * Created by Admin on 11/17/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class SeasonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Datum datum;
    private OnItemStateListener mListener;
    public SeasonAdapter(Context context, Datum datum) {
        mContext = context;
        this.datum = datum;
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.season_item, null)); //header_item_menu_left
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
        switch (datum.getType()){
            case 1:
                viewHolder.mName.setText("Liên Quan");
                break;
            case 3:
                if(position == 1)
                    viewHolder.mName.setText("Liên Quan");
                else
                    viewHolder.mName.setText("Tập Tiếp Theo");
                break;
            case 5:
                if(position == datum.getSeasons().size())
                    viewHolder.mName.setText("Liên Quan");
                else
                    viewHolder.mName.setText("Mùa " + datum.getSeasons().get(position).getIndex());
                break;
//            default:
//                viewHolder.mName.setText("Liên Quan");
        }
    }

    @Override
    public int getItemCount() {
        switch (datum.getType()){
            case 1:
                return 1; // phim lien quan
            case 3:
                return 2; // tap tiep theo, phim lien quan
            case 5:
                return datum.getSeasons().size() + 1; // seasons, phim lien quan
        }
        return 2;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mName;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.tv_season);
            mName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }
}