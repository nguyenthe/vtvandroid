package zazu.mobile.zazutv.adapter.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;
import zazu.mobile.zazutv.model.episodes.Episode;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.model.live.Live;

public class CategoryMaulCarouselAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnItemStateListener mListener;
    private List<Datum> mArrayList;
    private List<Live> mArrayChannels;
    private List<Episode> mArrayEpisodes;
    private boolean isChannel, isEpisode;
    private final static int CONTENT_VIEW = 1;

    public CategoryMaulCarouselAdapter(Context context, List<Datum> mArrayList) {
        this.mContext = context;
        this.mArrayList = mArrayList;
    }

    public List<Datum> getArrayList() {
        return mArrayList;
    }

    public CategoryMaulCarouselAdapter(Context context) {
        this.mContext = context;
    }

    public void setDataMovie(List<Datum> mArrayChannels) {
        this.mArrayList = mArrayChannels;
        this.isChannel = false;
        this.isEpisode = false;
        notifyDataSetChanged();
    }

    public void setDataChannel(List<Live> mArrayChannels) {
        this.mArrayChannels = mArrayChannels;
        this.isChannel = true;
        this.isEpisode = false;
        notifyDataSetChanged();
    }

    public void setDataEpisodes(List<Episode> mArrayChannels) {
        this.mArrayEpisodes = mArrayChannels;
        this.isChannel = false;
        this.isEpisode = true;
        notifyDataSetChanged();
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.item_adapter_horizontal, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RecyclerViewHolder) {
            String url = "";
            try {
                if (isChannel) {
                    url = mArrayChannels.get(position).getMobile_logo();
                } else if (isEpisode) {
                    url = mArrayEpisodes.get(position).getThumbnails().getLandscape().getUrls().get640360();
                } else {
                    url = mArrayList.get(position).getBackgroundImage();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(url))
                Picasso.with(mContext).load(url)
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.background_home)
                        .error(R.drawable.background_home)
                        .noFade()
                        .into(((RecyclerViewHolder) holder).mImageView);
//            ((RecyclerViewHolder) holder).mImageView.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        try {
            return isChannel ? mArrayChannels.size() : isEpisode ? mArrayEpisodes.size() : mArrayList.size();
        } catch (Exception e) {
            return 0;
        }
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        FocusRelativeLayout mRelativeLayout;
        ImageView mImageView;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mRelativeLayout = itemView.findViewById(R.id.fl_main_layout);
            mImageView = itemView.findViewById(R.id.img_Thumb);
            mRelativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }
}
