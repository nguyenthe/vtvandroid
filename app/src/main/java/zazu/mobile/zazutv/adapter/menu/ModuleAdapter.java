package zazu.mobile.zazutv.adapter.menu;

/**
 * Created by Admin on 11/16/2017.
 */

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.menuleft.Item;


public class ModuleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Item> mItemCount = new ArrayList<>();

    public ModuleAdapter(Context context, List<Item> mItemCount) {
        mContext = context;
        mItemCount = mItemCount;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.module_item_recyclerview, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
        viewHolder.mName.setText(mItemCount.get(position).getName());
        GradientDrawable drawable = (GradientDrawable) viewHolder.mFrameLayout.getBackground();
        drawable.setColor(ContextCompat.getColor(mContext, ContantUtil.getRandColor()));
    }

    @Override
    public int getItemCount() {
        return mItemCount.size();
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout mFrameLayout;
        TextView mName;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.tv_item_tip);
            mFrameLayout = (RelativeLayout) itemView.findViewById(R.id.fl_main_layout);
        }
    }
}