package zazu.mobile.zazutv.adapter.search;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

/**
 * Created by Admin on 9/22/2017.
 */

public class DemoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Datum> mArrayList;
    private List<Datum> mFilteredList;
    private final static int CONTENT_VIEW = 1;
    private Context context;

    public DemoAdapter(List<Datum> titles, Context context) {
        this.mArrayList = titles;
        this.context = context;
        this.mFilteredList = new ArrayList<Datum>();
        this.mFilteredList.addAll(this.mArrayList);
    }

    public interface PostItemListener {
        void onPostClick(View view, int position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == CONTENT_VIEW) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_card, viewGroup, false);
            return new ContentHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ContentHolder) {
            Picasso.with(context).load(mArrayList.get(position).getBackgroundImage())
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.background_home)
                    .error(R.drawable.background_home)
                    .noFade()
                    .into(((ContentHolder) holder).imageView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return CONTENT_VIEW;
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }


    public class ContentHolder extends RecyclerView.ViewHolder {
        ///////////////////////// HeadLines
        private ImageView imageView;

        public ContentHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.poster_iv);
        }
    }

    public void filterDemo(String query) {
        mFilteredList = new ArrayList<>();
        for (Datum datum : mArrayList) {
            if (datum.getTitle().toLowerCase().contains(query.toLowerCase())) {
                mFilteredList.add(datum);
            }
        }
        notifyDataSetChanged();
    }

    public void clear() {
        int size = this.mArrayList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.mFilteredList.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Clear the filter list
                mFilteredList.clear();
                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {
                    mFilteredList.addAll(mFilteredList);
                } else {
                    // Iterate in the original List and add it to filter list...
                    for (Datum item : mArrayList) {
                        if (item.getTitle().toLowerCase().contains(text.toLowerCase())) {
                            // Adding Matched items
                            mFilteredList.add(item);
                        }
                    }
                }
                // Set on UI Thread
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }
}
