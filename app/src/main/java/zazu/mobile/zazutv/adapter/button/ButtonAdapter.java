package zazu.mobile.zazutv.adapter.button;

/**
 * Created by Admin on 11/17/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;

public class ButtonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnItemStateListener mListener;

    public ButtonAdapter(Context context) {
        mContext = context;
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.button_rating, null)); //header_item_menu_left
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        FocusRelativeLayout mRelativeLayout;
        Button mName;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mName = (Button) itemView.findViewById(R.id.btn_Rate);
            mRelativeLayout = (FocusRelativeLayout) itemView.findViewById(R.id.fl_main_layout);
            mRelativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }
}