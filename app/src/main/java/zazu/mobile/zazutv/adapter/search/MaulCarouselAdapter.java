package zazu.mobile.zazutv.adapter.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;
import zazu.mobile.zazutv.model.homepage.theme.Datum;

public class MaulCarouselAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnItemStateListener mListener;
    private List<Datum> mArrayList;
    private final static int CONTENT_VIEW = 1;

    public MaulCarouselAdapter(Context context, List<Datum> mArrayList) {
        this.mContext = context;
        this.mArrayList = mArrayList;
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.item_adapter_horizontal, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RecyclerViewHolder) {
            if (!mArrayList.get(position).getBackgroundImage().equalsIgnoreCase("")) {
                Picasso.with(mContext).load(mArrayList.get(position).getBackgroundImage())
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.background_home)
                        .error(R.drawable.background_home)
                        .noFade()
                        .into(((RecyclerViewHolder) holder).mImageView);
//                ((RecyclerViewHolder) holder).mImageView.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        FocusRelativeLayout mRelativeLayout;
        ImageView mImageView;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mRelativeLayout = (FocusRelativeLayout) itemView.findViewById(R.id.fl_main_layout);
            mImageView = (ImageView) itemView.findViewById(R.id.img_Thumb);
            mRelativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }

}
