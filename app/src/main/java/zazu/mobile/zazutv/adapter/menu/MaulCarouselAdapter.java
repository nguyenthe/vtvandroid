package zazu.mobile.zazutv.adapter.menu;

/**
 * Created by Admin on 11/17/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;
import zazu.mobile.zazutv.model.menuleft.Item;

public class MaulCarouselAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Item> list = new ArrayList<Item>();
    private OnItemStateListener mListener;
    public MaulCarouselAdapter(Context context, List<Item> items) {
        mContext = context;
        this.list = items;
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(View.inflate(mContext, R.layout.header_category, null)); //header_item_menu_left
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
        viewHolder.mName.setText(list.get(position).getLabel().toUpperCase());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        FocusRelativeLayout mRelativeLayout;
        TextView mName;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.tv_item_tip);
            mRelativeLayout = itemView.findViewById(R.id.fl_main_layout);
            mRelativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }
}