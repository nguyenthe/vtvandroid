package zazu.mobile.zazutv.adapter.menu_icon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;
import zazu.mobile.zazutv.model.demo.Icon;

/**
 * Created by Admin on 11/19/2017.
 */

public class MenuIconAdapter extends RecyclerView.Adapter<MenuIconAdapter.PlanetViewHolder> {

    private List<Icon> dataList;
    private Context context;
    public MenuIconAdapter(List<Icon> planetList,Context context) {
        this.dataList = planetList;
        this.context = context;
    }

    @Override
    public MenuIconAdapter.PlanetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_icon_text,parent,false);
        PlanetViewHolder viewHolder=new PlanetViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MenuIconAdapter.PlanetViewHolder holder, int position) {
        holder.image.setImageResource(dataList.get(position).getIdSource());
        holder.text.setText(dataList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class PlanetViewHolder extends RecyclerView.ViewHolder{

        protected ImageView image;
        protected TextView text;
        protected FocusRelativeLayout mRelativeLayout;

        public PlanetViewHolder(View itemView) {
            super(itemView);
            image= (ImageView) itemView.findViewById(R.id.image_id);
            text= (TextView) itemView.findViewById(R.id.text_id);
            mRelativeLayout = (FocusRelativeLayout) itemView.findViewById(R.id.fl_main_layout);
        }
    }
}
