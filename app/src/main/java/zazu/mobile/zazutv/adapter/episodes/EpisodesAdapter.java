package zazu.mobile.zazutv.adapter.episodes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import zazu.mobile.zazutv.R;
import zazu.mobile.zazutv.adapter.menu.view.FocusRelativeLayout;
import zazu.mobile.zazutv.model.episodes.Episode;

public class EpisodesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnItemStateListener mListener;
    private List<Episode> mArrayList;
    private final static int CONTENT_VIEW = 1;

    public EpisodesAdapter(Context context, List<Episode> mArrayList) {
        this.mContext = context;
        this.mArrayList = mArrayList;
    }

    public void setOnItemStateListener(OnItemStateListener listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_episodes, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RecyclerViewHolder) {
            if (mArrayList.get(position) != null) {
                Episode datum = mArrayList.get(position);
                if (datum.getThumbnails().getLandscape() != null)
                    Picasso.with(mContext).load(datum.getThumbnails().getLandscape().getUrls().get1280720())
                            .placeholder(R.drawable.background_home)
                            .error(R.drawable.background_home)
                            .into(((RecyclerViewHolder) holder).mImageView);
                ((RecyclerViewHolder) holder).tvNameFilm.setText(datum.getName() + "");
                ((RecyclerViewHolder) holder).tvSumtime.setText(datum.getDurationText() + "");
                ((RecyclerViewHolder) holder).tvDescription.setText(datum.getDescription().toString());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImageView;
        TextView tvNameFilm;
        TextView tvSumtime;
        TextView tvDescription;
        FocusRelativeLayout mRelativeLayout;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.img_Thumb);
            tvNameFilm = (TextView) itemView.findViewById(R.id.name_Film);
            tvSumtime = (TextView) itemView.findViewById(R.id.tv_SumTime);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_Description);
            mRelativeLayout = (FocusRelativeLayout) itemView.findViewById(R.id.fl_main_layout);
            mRelativeLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemStateListener {
        void onItemClick(View view, int position);
    }

}
