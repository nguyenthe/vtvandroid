/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package zazu.mobile.zazutv;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zazu.mobile.zazutv.convert.ConvertUser;
import zazu.mobile.zazutv.model.demo.Icon;
import zazu.mobile.zazutv.model.homepage.Example;
import zazu.mobile.zazutv.model.homepage.theme.Datum;
import zazu.mobile.zazutv.model.homepage.themeupdate.Data;
import zazu.mobile.zazutv.presenter.IconHeaderPresenter;
import zazu.mobile.zazutv.presenter.MoviePresenter;
import zazu.mobile.zazutv.service.ApiUtils;
import zazu.mobile.zazutv.service.SOService;
import zazu.mobile.zazutv.service.common.Config;
import zazu.mobile.zazutv.sqlitehelper.DatabaseHandler;
import zazu.mobile.zazutv.ui.category.CategoryActivity;
import zazu.mobile.zazutv.ui.detail.MovieDetailsNewActivity;
import zazu.mobile.zazutv.ui.login.LoginActivity;
import zazu.mobile.zazutv.ui.logout.LogoutDialog;
import zazu.mobile.zazutv.ui.search.SearchZazuActivity;

public class MainFragment extends BrowseFragment {
  private static final String TAG = "MainFragment";
  private static final int REQUEST_LOGIN = 99;
  public static final int REQUEST_LOGOUT = 100;

  private ArrayObjectAdapter mRowsAdapter = null;
  private SOService soService;
  public ImageView imageView, imgIconHeader;
  public TextView tvTitle;
  public TextView tvTimeDuration;
  public TextView tvDescription;
  public TextView tvActor, tvDirector, tvGenre;
  public RelativeLayout rlSearch;
  public RelativeLayout rlInfoFilm;
  public ProgressBar progressBar;
  public ScrollView scrollView;
  public TextView tvTitleIconHeader;
  public View lnWatch;
  private DatabaseHandler databaseHandler;
  private String content = "";
  private static String KEY = "id";

  private boolean isFirstTime;
  private ArrayObjectAdapter gridRowAdapter;

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    Log.i(TAG, "onCreate");
    super.onActivityCreated(savedInstanceState);
    imageView = getActivity().findViewById(R.id.img_Thumb);
    tvTitle = getActivity().findViewById(R.id.tv_Label);
    tvTimeDuration = getActivity().findViewById(R.id.tv_TimeDuration);
    tvDescription = getActivity().findViewById(R.id.tv_Description);
    tvActor = getActivity().findViewById(R.id.tv_Actor);
    tvDirector = getActivity().findViewById(R.id.tv_Director);
    tvGenre = getActivity().findViewById(R.id.tv_Genre);
    rlSearch = getActivity().findViewById(R.id.rl_Search);
    rlInfoFilm = getActivity().findViewById(R.id.rl_InfoFilm);
    progressBar = getActivity().findViewById(R.id.progressbar);
    scrollView = getActivity().findViewById(R.id.scrollView);
    imgIconHeader = getActivity().findViewById(R.id.imgIconHeader);
    tvTitleIconHeader = getActivity().findViewById(R.id.tv_Search);
    lnWatch = getActivity().findViewById(R.id.ln_watch);
  }

  public static MainFragment newInstance(String someInt) {
    MainFragment myFragment = new MainFragment();
    Bundle args = new Bundle();
    args.putString(KEY, someInt);
    myFragment.setArguments(args);
    return myFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    soService = ApiUtils.getSOService();
    databaseHandler = new DatabaseHandler(getActivity());
    mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
    setupUIElements();
    //prepareBackgroundManager();
    //loadHeaderIcon();

    updateIcon();
    getData();

    setupEventListeners();
  }

  private void updateIcon() {
    ConvertUser user = checkStatusLogin();
    if (user != null) {
      updateIconApp(true);
      publishProgress1(user.getToken());
    } else
      updateIconApp(false);
  }

  private void getData() {
    soService.getDataHomePage().enqueue(new Callback<Example>() {
      @Override
      public void onResponse(Call<Example> call, final Response<Example> response) {
        Log.e(TAG, "onResponse : " + call.request() + " --- " + response.code());
        if (response.code() == 200) {
          List<Data> items = response.body().getData();
          for (int i = 0; i < items.size(); i++) {
            publishProgress(items.get(i));
          }
        }
      }

      @Override
      public void onFailure(Call<Example> call, Throwable t) {
        Log.e(TAG, "onFailure getLog: " + call.request() + " --- " + t.getMessage());
      }
    });
  }

  private ConvertUser checkStatusLogin() {
    try {
      return databaseHandler.getAllUser().get(0);
    } catch (Exception e) {
      Log.e(TAG, " myList " + e.getMessage());
    }
    return null;
  }


  private void publishProgress1(String token) {
    final HeaderItem headerItem = new HeaderItem("My List");
    MoviePresenter moviePresenterMyList = new MoviePresenter();
    final ArrayObjectAdapter cardRowAdapterMyList = new ArrayObjectAdapter(moviePresenterMyList);
    soService.getMyListDetail(Config.idToken + token).enqueue(new Callback<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example>() {
      @Override
      public void onResponse(Call<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example> call, Response<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example> response) {
        Log.e(TAG, " get data mylist " + response.code());
        try {
          if (response.isSuccessful()) {
            List<Datum> list = response.body().getData().getItems();
            if (list != null && !list.isEmpty()) {
              mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());

              for (int i = 0; i < list.size(); i++) {
                cardRowAdapterMyList.add(list.get(i));
              }
              mRowsAdapter.add(new ListRow(headerItem, cardRowAdapterMyList));
              setAdapter(mRowsAdapter);
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onFailure(Call<zazu.mobile.zazutv.model.homepage.mylist.detailmylist.Example> call, Throwable t) {
      }
    });
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK) {
      updateIconApp(requestCode == REQUEST_LOGIN);
    }
  }

  private void publishProgress(Data data) {
    MoviePresenter moviePresenter = new MoviePresenter();
    final ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(moviePresenter);
    for (int i = 0; i < data.getItems().size(); i++) {
      cardRowAdapter.add(data.getItems().get(i));
    }
    HeaderItem header = new HeaderItem(data.getObjectDetail().getTitle());
    mRowsAdapter.add(new ListRow(header, cardRowAdapter));
    setAdapter(mRowsAdapter);
    if (!isFirstTime && !data.getItems().isEmpty() && getRowsFragment() != null) {
      isFirstTime = true;
      getRowsFragment().setSelectedPosition(1);
    }
//    soService.getListFromBlock(item.getId().toString()).enqueue(new Callback<zazu.mobile.zazutv.model.homepage.themeupdate.Example>() {
//      @Override
//      public void onResponse(Call<zazu.mobile.zazutv.model.homepage.themeupdate.Example> call,
//                             Response<zazu.mobile.zazutv.model.homepage.themeupdate.Example> response) {
//        if (response.body().getData().getItems() != null) {
//          List<Datum> list = response.body().getData().getItems();
//          for (int i = 0; i < list.size(); i++) {
//            cardRowAdapter.add(list.get(i));
//          }
//          HeaderItem header = new HeaderItem(item.getTitle());
//          mRowsAdapter.add(new ListRow(header, cardRowAdapter));
//          setAdapter(mRowsAdapter);
//          if (!isFirstTime && !list.isEmpty() && getRowsFragment() != null) {
//            isFirstTime = true;
//            getRowsFragment().setSelectedPosition(1);
//          }
//        }
//      }
//
//      @Override
//      public void onFailure(Call<zazu.mobile.zazutv.model.homepage.themeupdate.Example> call, Throwable t) {
//        Log.e(TAG, "onFailure : " + call.request() + " --- " + t.getMessage());
//
//      }
//    });
  }

  public void updateIconApp(boolean isLogin) {
    mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
    HeaderItem gridItemPresenterHeader = new HeaderItem(0, "");
    IconHeaderPresenter mGridPresenter = new IconHeaderPresenter();
    if (gridRowAdapter == null) {
      gridRowAdapter = new ArrayObjectAdapter(mGridPresenter);

      Icon icon = new Icon((R.drawable.icon_live_bg), R.drawable.live_icon_white, getString(R.string.live));
      gridRowAdapter.add(icon);

      icon = new Icon((R.drawable.icon_movie_bg), R.drawable.movie_icon_white, getString(R.string.movie).toUpperCase());
      gridRowAdapter.add(icon);

      icon = new Icon((R.drawable.icon_tv_show_bg), R.drawable.tv_show_icon_white, getString(R.string.tv_show));
      gridRowAdapter.add(icon);

      icon = new Icon((R.drawable.icon_search_bg), R.drawable.search_icon_white, getString(R.string.search));
      gridRowAdapter.add(icon);

      if (isLogin) {
        icon = new Icon((R.drawable.icon_logout_bg), R.drawable.logout_icon_white, getString(R.string.logout));
      } else {
        icon = new Icon((R.drawable.icon_login_bg), R.drawable.login_icon_white, getString(R.string.login));
      }
      gridRowAdapter.add(icon);

      mRowsAdapter.add(new ListRow(gridItemPresenterHeader, gridRowAdapter));
      setAdapter(mRowsAdapter);
    } else {
      Icon icon;
      if (isLogin) {
        icon = new Icon((R.drawable.icon_logout_bg), R.drawable.logout_icon_white, getString(R.string.logout));
//        gridRowAdapter.add(icon);
      } else {
        icon = new Icon((R.drawable.icon_login_bg), R.drawable.login_icon_white, getString(R.string.login));
//        gridRowAdapter.add(icon);
      }
      gridRowAdapter.replace(4, icon);
//      mMenuAdapter.notifyAll();
    }
  }

  private void setupUIElements() {
    // over title
    setHeadersState(HEADERS_DISABLED); //HEADERS_DISABLED
  }

  private void setupEventListeners() {
    setOnItemViewClickedListener(new ItemViewClickedListener());
    setOnItemViewSelectedListener(new ItemViewSelectedListener());
  }

  private final class ItemViewClickedListener implements OnItemViewClickedListener {
    @Override
    public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                              RowPresenter.ViewHolder rowViewHolder, Row row) {
      if (item instanceof Datum) {
        watchMovie((Datum) item);
      } else if (item instanceof Icon) {
        if (((Icon) item).getTitle().equalsIgnoreCase(getActivity().getResources().getString(R.string.live))) {
          startActivity(CategoryActivity.getInstance(getActivity(), CategoryActivity.TYPE_CHANNEL));
        } else if (((Icon) item).getTitle().equalsIgnoreCase(getActivity().getResources().getString(R.string.search))) {
          Intent intent = new Intent(getActivity(), SearchZazuActivity.class);
          startActivity(intent);
        } else if (((Icon) item).getTitle().equalsIgnoreCase(getActivity().getResources().getString(R.string.movie))) {
          startActivity(CategoryActivity.getInstance(getActivity(), CategoryActivity.TYPE_MOVIE));
        } else if (((Icon) item).getTitle().equalsIgnoreCase(getActivity().getResources().getString(R.string.login))) {
          startActivityForResult(LoginActivity.getInstance(getActivity()), REQUEST_LOGIN);
        } else if (((Icon) item).getTitle().equalsIgnoreCase(getActivity().getResources().getString(R.string.logout))) {
          DialogFragment newFragment = new LogoutDialog();
          newFragment.setTargetFragment(MainFragment.this, REQUEST_LOGOUT);
          newFragment.show(getFragmentManager(), "logout");
        }
      }
    }
  }

  private void watchMovie(Datum item) {
    Intent intent = new Intent(getActivity(), MovieDetailsNewActivity.class);
    intent.putExtra(MovieDetailsNewActivity.DATUM, item);
    getActivity().startActivity(intent);
    (getActivity()).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
  }

  private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
    @Override
    public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                               RowPresenter.ViewHolder rowViewHolder, Row row) {
      if (item instanceof Datum) {
        if (imageView != null) {
          imageView.setVisibility(View.VISIBLE);
          rlInfoFilm.setVisibility(View.VISIBLE);
          rlSearch.setVisibility(View.GONE);
          //Log.d(TAG, "get URl Image 1: " + ((Datum) item).getBackgroundImage());
          showDatumInfo((Datum) item);

        }
      } else if (item instanceof Icon) {
        Log.d(TAG, "get conent of icon " + ((Icon) item).getTitle());
        rlInfoFilm.setVisibility(View.GONE);
        rlSearch.setVisibility(View.VISIBLE);
        tvTitleIconHeader.setText(((Icon) item).getTitle());
        imgIconHeader.setImageResource(((Icon) item).getIdSourceWhite());
      }
    }
  }

  private void showDatumInfo(final Datum item) {
    if (item.getCovers() != null) {
      if (item.getCovers().getLandscape() != null) {
        Picasso.with(getActivity())
                .load(item.getCovers().getLandscape().getUrls().get1280720())
                .placeholder(R.drawable.background_home)
                .error(R.drawable.background_home)
                .into(imageView);

      }
    }
    tvTitle.setText(item.getTitle());
    tvTimeDuration.setText(item.getDurationText().replace("s", "").replaceAll("[^\\d.]", ":"));
    tvDescription.setText(Html.fromHtml(item.getDescription()));
    if (!TextUtils.isEmpty(item.getActors())) {
      tvActor.setText(String.format(getString(R.string.cast), item.getActors()));
      tvActor.setVisibility(View.VISIBLE);
    } else
      tvActor.setVisibility(View.GONE);
    if (!TextUtils.isEmpty(item.getGenresText())) {
      tvGenre.setText(item.getGenresText());
      tvGenre.setVisibility(View.VISIBLE);
    } else
      tvGenre.setVisibility(View.GONE);
    if (!TextUtils.isEmpty(item.getDirectors())) {
      tvDirector.setText(String.format(getString(R.string.director), item.getDirectors()));
      tvDirector.setVisibility(View.VISIBLE);
    } else
      tvDirector.setVisibility(View.GONE);
    lnWatch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        watchMovie(item);
      }
    });
  }
}
